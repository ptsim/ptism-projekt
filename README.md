# PTISM - Projekt

## Prerequisites

### 1. Install:
  - Node (version >= 10 atleast)
  - yarn
  - Gradle
  - Java 11
  - Docker (when running in production mode)
  
### 2. Edit /etc/hosts file with:

#### Linux:
Add `127.0.0.1 www.ptism.test` to `/etc/hosts`

#### Windows
Add `127.0.0.1 www.ptism.test` to `%SystemRoot%\system32\drivers\etc\hosts` 

## Available run command:

In order to be able to pull latest backend image you need to login into registry.
 
Run `docker login registry.gitlab.com` 
and provide your Gitlab's credentials.

Now you can use:

* `./start_remote.sh`
to pull newest required Docker images 

or

* `./start_local.sh`
to build services locally

and run all services.

To see running containers run: `docker ps` <br />
GUI available at : [https://www.ptism.test/](https://www.ptism.test/)




