#!/bin/bash

set -e

SOURCE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function app_prod_start() (
    echo "Building DOCTORD backend local docker image..."

    cd $SOURCE_DIR/../application/backend/

    ./gradlew jibDockerBuild

    echo "Backend docker image successfully built."

    cd ../frontend/

    yarn install && yarn build
    cd ../../

    echo "Starting prod version of DOCTORD..."

    docker-compose -f docker/docker-prod-compose.yml up

    echo "DOCTORD started."
)

app_prod_start