#!/bin/bash

set -e

SOURCE_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function app_start() (
    echo "Pulling DOCTORD backend newest backend image..."

    cd $SOURCE_DIR/../

    docker pull registry.gitlab.com/ptsim/ptism-projekt:latest

    echo "Starting backend..."

    docker-compose -f docker/docker-compose.yml up -d

    echo "DOCTORD started."
)

app_start
