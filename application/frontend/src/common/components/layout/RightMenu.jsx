/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import RealmRolesService from 'common/authorization/RealmRolesService';
import realmRoles from 'common/authorization/realmRoles';
import CurrentTime from 'common/components/layout/CurrentTime';
import ProfileCircle from 'common/components/layout/ProfileCircle';
import CreateRoomButton from 'common/components/layout/CreateRoomButton';
import VerticalLineRight from 'common/components/layout/VerticalLineRight';
import settingsIcon from 'images/icons/settings.svg';

const RightMenuWrapper = styled.div.attrs({ className: 'right-menu-wrapper' })`
  float: right;
  display: inline-block;
  height: 70px;
  line-height: 80px;
  font-size: 14px;
  font-family: 'Titillium Web', sans-serif;
  margin-right: 40px;

  @media only screen and (max-width: 1120px) {
    float: none;
  }

  @media only screen and (max-width: 970px) {
    margin-right: 0;
  }
`;

const TopMenuWrapper = styled.ul.attrs({ className: 'top-menu-wrapper' })`
  margin: 0;
  padding: 0;
  list-style-type: none;
  display: inline-block;
  margin-right: 20px;
`;

const TopMenuItem = styled.li.attrs({ className: 'top-menu-item' })`
  margin: 0;
  padding: 0;
  cursor: pointer;
  margin-left: 15px;
  display: inline-block;
  position: relative;
`;

const CreateRoomButtonWrapper = styled.div.attrs({ className: 'create-room-button-wrapper' })`
  display: inline-block;
  float: left;
`;

const TopMenuImg = styled.img.attrs({ className: 'top-menu-img', alt: 'top-menu-img' })`
  margin: 0;
  width: 20px;
  height: 20px;
`;

class RightMenu extends Component {
  showCreateRoomButton = () =>
    RealmRolesService.hasAnyRole([realmRoles.ROLE_ADMIN, realmRoles.ROLE_DOCTOR]);

  render() {
    return (
      <RightMenuWrapper>
        {this.showCreateRoomButton() ? (
          <CreateRoomButtonWrapper>
            <CreateRoomButton />
          </CreateRoomButtonWrapper>
        ) : null}
        <TopMenuWrapper>
          <TopMenuItem>
            <Link to="/settings">
              <TopMenuImg src={settingsIcon} />
            </Link>
          </TopMenuItem>
        </TopMenuWrapper>
        <ProfileCircle onLogout={this.props.onLogout} username={this.props.username} />
        <VerticalLineRight />
        <CurrentTime />
        <VerticalLineRight />
      </RightMenuWrapper>
    );
  }
}

RightMenu.propTypes = {
  onLogout: PropTypes.func.isRequired,
  username: PropTypes.string.isRequired,
};

export default RightMenu;
