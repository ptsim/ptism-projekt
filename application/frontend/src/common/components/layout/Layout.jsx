import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Header from 'common/components/layout/Header';
import HeaderLogo from 'common/components/layout/HeaderLogo';
import SideBar from 'common/components/layout/SideBar';
import { logoutUser } from 'common/actions/authUserActions';
import { connect } from 'react-redux';
import Colors from 'common/colors';
import DisableOverlay, { applyStyle } from 'common/components/layout/DisableOverlay';
import ReactNotification from 'react-notifications-component';

const LayoutWrapper = styled.div.attrs({ className: 'layout-wrapper' })`
  display: grid;
  grid-template-columns: 1fr 7fr;
  grid-template-rows: 1fr 15fr;
  grid-template-areas:
    'header-logo-wrapper header-area'
    'side-bar-wrapper main-content-wrapper';
  height: 100%;
  width: 100%;
  margin: 0;
  padding: 0;

  overflow: hidden;
`;

const MainContentWrapper = styled.div.attrs({ className: 'main-content-wrapper' })`
  margin: 0;
  grid-area: main-content-wrapper;
  background: ${Colors.BACKGROUND_COLOR};
`;

const Layout = ({ isDisabled, username, children, logoutUserFunc }) => (
  <LayoutWrapper style={applyStyle(isDisabled)}>
    {isDisabled && <DisableOverlay />}
    <HeaderLogo />
    <Header username={username} onLogout={logoutUserFunc} />
    <SideBar />
    <MainContentWrapper>{children}</MainContentWrapper>
    <ReactNotification />
  </LayoutWrapper>
);

Layout.propTypes = {
  children: PropTypes.instanceOf(Object).isRequired,
  logoutUserFunc: PropTypes.func.isRequired,
  username: PropTypes.string.isRequired,
  isDisabled: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  username: state.common.authUser.keycloakInfo.userInfo.preferred_username,
  isDisabled: state.common.applicationScreen.locked,
});

const mapDispatchToProps = dispatch => ({
  logoutUserFunc: () => dispatch(logoutUser()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
