import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TimeRangeSlider from 'react-time-range-slider';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import GenericModal from 'common/components/GenericModal';
import TextInput from 'common/components/TextInput';
import ButtonProgressIndicator from 'common/components/ButtonProgressIndicator';
import { createRoom, createRoomSetDefault, checkRoomName } from 'common/actions/createRoomActions';

const CreateRoomFormWrapper = styled.div.attrs({ className: 'create-room-form-wrapper' })``;

const CreateFormModal = styled.div.attrs({ className: 'create-room-modal' })`
  line-height: 20px;
  max-width: 700px;
`;

const ModalTitle = styled.p.attrs({ className: 'modal-title' })`
  margin: 0 0 10px 0;
  font-size: 20px;
`;

const CreateSuccessTitle = styled.p.attrs({ className: 'create-success-title' })`
  display: block;
  margin: 0 0 30px 0;
  font-size: 15px;
  text-align: center;
  line-height: 20px;
  font-family: 'Roboto', sans-serif !important;
  font-weight: 100;
`;

const ErrorBlock = styled.div.attrs({ className: 'error-block' })`
  padding: 10px;
  margin: 5px 0;
  text-align: center;
  background: #fce7e6;
  font-size: 12px;
  color: #552526;
`;

const DatePickerBox = styled.div.attrs({ className: 'date-picker-box' })`
  margin-bottom: 5px;
`;

const DatePickerBoxTitle = styled.p.attrs({ className: 'date-picker-box-title' })`
  margin: 0 0 5px 0;
  font-size: 14px;
  color: #59637d;
  font-family: 'Titillium Web', sans-serif;
  display: inline-block;
`;

const SendButton = styled.button.attrs({ className: 'send-button' })`
  padding: 10px 20px;
  font-size: 12px;
  border-radius: 5px;
  border: 1px solid #f0f0f0;
  background: #2d4564;
  color: #fff;
  cursor: pointer;
  outline: none;
  width: 100%;
  margin-top: 41px;

  &:disabled {
    opacity: 0.6;
  }

  &:disabled:hover {
    cursor: auto;
  }
`;

const CreateRoomSuccessBox = styled.div.attrs({ className: 'create-room-success-box' })`
  padding: 25px 25px 15px 25px;
`;

const GoToRoomButton = styled(Link).attrs({ className: 'go-to-room-button' })`
  width: 100%;
  color: #fff;
  outline: none;
  border: none;
  border-radius: 4px;
  background: #2d4564;
  padding: 15px;
  font-size: 12px;
  transition: 0.2s;

  &:hover {
    opacity: 0.8;
  }
`;

const CreateRoomForm = ({
  isLoading,
  isError,
  isSuccess,
  nameTaken,
  checkRoomNameFunc,
  createRoomSetDefaultFunc,
  createRoomFunc,
  onClose,
  createdRoomId,
}) => {
  const [name, setName] = useState('');
  const [timeValue, setTimeValue] = useState({ start: '7:00', end: '21:00' });
  const date = new Date();

  const onSend = () => {
    const startTimeArray = timeValue.start.split(':');
    const endTimeArray = timeValue.end.split(':');

    const workStart = new Date(date);
    workStart.setHours(startTimeArray[0]);
    workStart.setMinutes(startTimeArray[1]);
    workStart.setMilliseconds(0);
    workStart.setSeconds(0);

    const workEnd = new Date(date);
    workEnd.setHours(endTimeArray[0]);
    workEnd.setMinutes(endTimeArray[1]);
    workEnd.setMilliseconds(0);
    workEnd.setSeconds(0);

    const objectToSend = {
      name,
      workStart,
      workEnd,
    };

    createRoomFunc(objectToSend);
  };

  const onCloseModal = () => {
    createRoomSetDefaultFunc();
    onClose();
  };

  const onRoomNameChange = (itemName, value) => {
    setName(value);
    if (value.length > 3) checkRoomNameFunc(value);
  };

  const canSendForm = () => name.length !== 0 && !nameTaken;

  const timeChangeHandler = time => {
    setTimeValue(time);
  };

  return (
    <GenericModal onClose={onCloseModal}>
      {isSuccess ? (
        <CreateRoomSuccessBox>
          <CreateSuccessTitle>Pokój został stworzony!</CreateSuccessTitle>
          <div className="success-checkmark">
            <div
              style={
                !isSuccess
                  ? {
                      display: 'none',
                    }
                  : {}
              }
              className="check-icon"
            >
              <span className="icon-line line-tip" />
              <span className="icon-line line-long" />
              <div className="icon-circle" />
              <div className="icon-fix" />
            </div>
          </div>
          <GoToRoomButton to={`/room/${createdRoomId}`}>Przejdź do pokoju</GoToRoomButton>
        </CreateRoomSuccessBox>
      ) : (
        <CreateFormModal>
          <ModalTitle>Stwórz nowy pokój</ModalTitle>
          <ErrorBlock
            style={
              isError || nameTaken
                ? {}
                : {
                    display: 'none',
                  }
            }
          >
            {isError ? 'Wystąpił problem podczas tworzenia pokoju. Spróbuj ponownie.' : ''}
            {nameTaken ? 'Nazwa pokoju jest juz zajęta!' : ''}
          </ErrorBlock>
          <CreateRoomFormWrapper>
            <TextInput
              id="roomName"
              name="roomName"
              onChange={onRoomNameChange}
              label="Nazwa pokoju"
              validator={() => true}
            />
            <DatePickerBox>
              <DatePickerBoxTitle>
                Czas pracy: {timeValue.start} - {timeValue.end}
              </DatePickerBoxTitle>
              <TimeRangeSlider
                maxValue="21:00"
                minValue="7:00"
                name="time_range"
                onChange={timeChangeHandler}
                step={15}
                value={timeValue}
              />
            </DatePickerBox>
            <SendButton disabled={!canSendForm || isLoading} onClick={onSend}>
              {isLoading ? <ButtonProgressIndicator /> : 'Stwórz'}
            </SendButton>
          </CreateRoomFormWrapper>
        </CreateFormModal>
      )}
    </GenericModal>
  );
};

CreateRoomForm.propTypes = {
  onClose: PropTypes.func.isRequired,
  createRoomFunc: PropTypes.func.isRequired,
  createRoomSetDefaultFunc: PropTypes.func.isRequired,
  checkRoomNameFunc: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  isSuccess: PropTypes.bool.isRequired,
  nameTaken: PropTypes.bool.isRequired,
  createdRoomId: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  isLoading: state.common.createRoom.isLoading,
  isError: state.common.createRoom.isError,
  isSuccess: state.common.createRoom.isSuccess,
  wasNameChecked: state.common.createRoom.wasNameChecked,
  isNameLoading: state.common.createRoom.isNameLoading,
  nameTaken: state.common.createRoom.nameTaken,
  createdRoomId: state.common.createRoom.createdRoomId,
});

const mapDispatchToProps = dispatch => ({
  createRoomFunc: data => dispatch(createRoom(data)),
  checkRoomNameFunc: roomName => dispatch(checkRoomName(roomName)),
  createRoomSetDefaultFunc: () => dispatch(createRoomSetDefault()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateRoomForm);
