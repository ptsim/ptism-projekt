import React from 'react';
import styled from 'styled-components';

const VerticalLineWrapper = styled.div.attrs({ className: 'vertical-line-wrapper' })`
  width: 1px;
  height: 50px;
  border-right: 1px solid #f0f0f0;
  display: inline-block;
  margin-top: 10px;
  float: right;
`;

const VerticalLineRight = () => <VerticalLineWrapper />;

export default VerticalLineRight;
