import React, { useState } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import SideFooter from 'common/components/layout/SideFooter';
import IntegrateApps from 'common/components/layout/IntegrateApps';
import homeIcon from 'images/icons/home.svg';
import lessonIcon from 'images/icons/lesson.svg';
import messagesIcon from 'images/icons/messages.svg';
import calendarIcon from 'images/icons/calendar.svg';
import Colors from 'common/colors';

const SideBarWrapper = styled.div.attrs({ className: 'side-bar-header-wrapper' })`
  grid-area: side-bar-wrapper;
  background: ${Colors.WHITE};
  position: relative;
  border-right: 1px solid ${Colors.GALLERY};
  max-height: 90vh;
  min-width: 80px;
`;

const SideBarMenuWrapper = styled.ul.attrs({ className: 'sidebar-menu-wrapper' })`
  list-style-type: none;
  margin: 0;
  width: 100;
  padding: 0;
  font-family: 'Roboto-Light', sans-serif;
`;

const SideBarMenuItemWrapper = styled.li.attrs({ className: 'sidebar-menu-item-wrapper' })`
  margin: 0;
  background: ${Colors.WHITE};
  border-right: none;
  text-align: center;
  font-size: 14px;
  padding: 0;
  font-weight: bold;
  color: ${Colors.LIGHT_BLACK};
  transition: 0.2s;

  &:first-child {
    border-top: none;
  }

  &:hover {
    cursor: pointer;
    color: #4369a2 !important;
    border-right: 3px solid #4369a2;
    background: #f3f7fd;
  }

  &:hover img {
    filter: invert(0.1) sepia(100%) hue-rotate(190deg) saturate(500%) !important;
  }
`;

const SideBarItemImg = styled.img.attrs({ className: 'sidebar-item-img', alt: 'sidebar-item-img' })`
  display: inline-block;
  margin: 0 auto;
  width: 24px;
  height: 24px;
  grid: img-area;
`;

const StyledLink = styled(Link).attrs({ className: 'sidebar-styled-link' })`
  font-size: 14px;
  line-height: 24px;
  padding: 15px 0;
  font-weight: bold;
  text-decoration: none;
  display: grid;
  grid-template-columns: 1fr 2fr;
  line-height: 24px;
  grid-template-areas: 'img-area text-area';

  @media only screen and (max-width: 920px) {
    grid-template-areas: 'img-area';
    grid-template-columns: 1fr;
  }
`;

const SideBarItemText = styled.div.attrs({ className: 'sidebar-item-text' })`
  grid: text-area;
  text-align: left;
  font-size: 12px;
  color: #333;

  @media only screen and (max-width: 920px) {
    display: none;
  }
`;

const MENU_ICONS = [
  {
    id: 1,
    link: '/',
    name: 'Główna',
    icon: homeIcon,
  },
  {
    id: 2,
    link: '/visit',
    name: 'Wizyta',
    icon: lessonIcon,
  },
  {
    id: 3,
    link: '/history',
    name: 'Historia wizyt',
    icon: calendarIcon,
  },
  {
    id: 4,
    link: '/messages',
    name: 'Wiadomości',
    icon: messagesIcon,
  },
];

const activeTabStyle = {
  cursor: 'pointer',
  color: '#4369a2',
  borderRight: '3px solid #4369a2',
  background: '#f3f7fd',
};

const activeImageStyle = {
  filter: 'filter: invert(0.1) sepia(100%) hue-rotate(190deg) saturate(500%) !important',
};

const SideBar = () => {
  const [activeTab, setActiveTab] = useState(`/${window.location.href.split('/').pop()}`);

  const compareStrings = (stringOne, stringTwo) =>
    stringOne.toUpperCase() === stringTwo.toUpperCase();

  const isActiveTab = item =>
    compareStrings(item.link, activeTab) ||
    (window.location.href.includes('/visit/') && item.id === 3);

  const renderMenuIcons = () => (
    <SideBarMenuWrapper>
      {MENU_ICONS.map(item => (
        <SideBarMenuItemWrapper
          onClick={() => setActiveTab(item.link)}
          key={item.id}
          style={isActiveTab(item) ? activeTabStyle : {}}
        >
          <StyledLink to={item.link}>
            <SideBarItemImg src={item.icon} style={isActiveTab(item) ? activeImageStyle : {}} />
            <SideBarItemText>{item.name}</SideBarItemText>
          </StyledLink>
        </SideBarMenuItemWrapper>
      ))}
    </SideBarMenuWrapper>
  );

  return (
    <SideBarWrapper>
      {renderMenuIcons()}
      <IntegrateApps />
      <SideFooter />
    </SideBarWrapper>
  );
};

export default SideBar;
