import React, { Component } from 'react';
import styled from 'styled-components';

const CurrentTimeWrapper = styled.div.attrs({ className: 'current-time-wrapper' })`
  display: inline-block;
  height: 70px;
  line-height: 70px;
  float: right;
  font-size: 12px;
  margin: 0 15px;
`;

class CurrentTime extends Component {
  state = {
    time: new Date()
      .toLocaleString()
      .split(' ')
      .slice(1, 2)
      .join(' '),
  };

  componentDidMount() {
    this.intervalID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.intervalID);
  }

  tick = () =>
    this.setState({
      time: new Date()
        .toLocaleString()
        .split(' ')
        .slice(1, 2)
        .join(' '),
    });

  render() {
    return <CurrentTimeWrapper>{this.state.time}</CurrentTimeWrapper>;
  }
}

export default CurrentTime;
