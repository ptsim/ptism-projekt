import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const TextInputBox = styled.div.attrs({ className: 'text-input-box' })`
  margin-bottom: 15px;
`;

const TextInputComponent = styled.input.attrs({ className: 'text-input-component' })`
  outline: none;
  background: none;
  border-radius: 4px;
  border: 1px solid #f0f0f0;
  padding: 10px;
  width: 90%;
  background: #fcfdfe;

  &:focus {
    border: 1px solid #6c89e6;
  }
`;

const TextInputText = styled.p.attrs({ className: 'text-input-text' })`
  margin: 0 0 5px 0;
  font-size: 14px;
  color: #59637d;
  font-family: 'Titillium Web', sans-serif;
  display: inline-block;
`;

const TextInputError = styled.p.attrs({ className: 'text-input-error' })`
  color: red;
  font-size: 10px;
  display: inline-block;
  margin: 0 22px 0 10px;
  float: right;
`;

const ERROR_STYLE = {
  border: '1px solid red',
};

class TextInput extends Component {
  state = {
    inputText: '',
    isError: false,
    errorText: '',
  };

  onChange = event => {
    const {
      target: { value, name },
    } = event;

    this.setState(
      {
        inputText: value,
      },
      () => {
        if (!this.props.validator(this.state.inputText)) {
          this.setState({ isError: true, errorText: 'Invalid value entered' });
        } else {
          this.setState({ isError: false, errorText: '' });
        }

        this.props.onChange(name, value);
      },
    );
  };

  render() {
    const { label, id, style, type, name } = this.props;
    const { inputText, isError, errorText } = this.state;

    return (
      <TextInputBox style={style}>
        <TextInputText>{label}</TextInputText>
        {isError ? <TextInputError>{errorText}</TextInputError> : null}

        <TextInputComponent
          type={type}
          style={isError ? ERROR_STYLE : {}}
          id={id}
          value={inputText}
          name={name}
          onChange={this.onChange}
        />
      </TextInputBox>
    );
  }
}

TextInput.defaultProps = {
  style: {},
  type: 'text',
};

TextInput.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  validator: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  style: PropTypes.instanceOf(Object),
  type: PropTypes.string,
};

export default TextInput;
