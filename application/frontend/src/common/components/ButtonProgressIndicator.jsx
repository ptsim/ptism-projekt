import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import buttonProgressAnimation from 'images/button_indicator.svg';

const ButtonImageComponent = styled.img.attrs({
  className: 'button-progress-component',
})``;

const ButtonProgressIndicator = ({ size }) => (
  <ButtonImageComponent
    style={{ height: `${size}px` }}
    src={buttonProgressAnimation}
    alt="buttonProgressAnimation"
  />
);

ButtonProgressIndicator.defaultProps = {
  size: 8,
};

ButtonProgressIndicator.propTypes = {
  size: PropTypes.number,
};

export default ButtonProgressIndicator;
