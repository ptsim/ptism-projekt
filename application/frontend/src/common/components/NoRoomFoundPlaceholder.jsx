import React from 'react';
import Placeholder from 'common/components/Placeholder';
import errorImage from 'images/not_found.svg';

const TITLE = 'Nie znaleziono pokoju.';
const SUBTITLE = 'Pokój z danym identyfikatorem nie został odnaleziony.';
const ALT = 'No room was found with given id';
const CUSTOM_CLASS_NAME = 'no-room-found-placeholder';

const NoRoomFoundPlaceholder = () => (
  <Placeholder
    src={errorImage}
    alt={ALT}
    title={TITLE}
    subtitle={SUBTITLE}
    customClassName={CUSTOM_CLASS_NAME}
  />
);

export default NoRoomFoundPlaceholder;
