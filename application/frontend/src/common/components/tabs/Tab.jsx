import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const TabItem = styled.li.attrs({ className: 'tab-item' })`
  margin: 0 20px;
  padding: 0 0 20px 0;
  text-align: center;
  display: inline-block;
  cursor: pointer;
  border-bottom: ;
  font-size: 14px;
  font-family: 'Titillium Web', sans-serif;
  color: #919fac;

  &:hover {
    border-bottom: 2px solid #456aa0;
    color: #2e3b5c;
  }
`;

const ACTIVE_TAB = { borderBottom: '2px solid #456aa0', color: '#2e3b5c' };

class Tab extends Component {
  static propTypes = {
    activeTab: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
  };

  onClick = () => {
    const { label, onClick } = this.props;
    onClick(label);
  };

  render() {
    const {
      onClick,
      props: { activeTab, label },
    } = this;

    return (
      <TabItem style={activeTab === label ? ACTIVE_TAB : {}} onClick={onClick}>
        {label}
      </TabItem>
    );
  }
}

export default Tab;
