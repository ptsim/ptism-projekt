import styled from 'styled-components';

const Title = styled.div.attrs({ className: 'n-title' })`
  font-family: 'Arial';
  font-size: 24px;
  font-weight: 300;
  line-height: 56px;
  padding: 0;
  margin-top: 20px;
`;

export default Title;
