import styled from 'styled-components';
import Color from 'common/colors';

const Subtitle = styled.div.attrs({ className: 'n-subtitle' })`
  font-family: 'Arial';
  font-size: 13px;
  padding: 0;
  color: ${Color.GRAY};
`;

export default Subtitle;
