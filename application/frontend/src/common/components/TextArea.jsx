import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const TextAreaWrapper = styled.div.attrs({ className: 'text-area-wrapper' })`
  margin-bottom: 15px;
`;

const TextAreaComponent = styled.textarea.attrs({ className: 'text-area-component' })`
  padding: 10px;
  width: 90%;
  outline: none;
  border: 1px solid #f0f0f0;
  background: none;
  border-radius: 4px;
  resize: none;

  &:focus {
    border: 1px solid #6c89e6;
  }
`;

const TextAreaLabel = styled.p.attrs({ className: 'text-area-text' })`
  margin: 0 0 5px 0;
  font-size: 14px;
  color: #59637d;
  font-family: 'Titillium Web', sans-serif;
`;

class TextArea extends Component {
  state = {
    inputText: '',
  };

  onChange = event => {
    const {
      target: { value, name },
    } = event;

    this.setState({
      inputText: value,
    });

    this.props.onChange(name, value);
  };

  render() {
    const { label, name } = this.props;
    const { inputText } = this.state;

    return (
      <TextAreaWrapper>
        <TextAreaLabel>{label}</TextAreaLabel>
        <TextAreaComponent value={inputText} name={name} onChange={this.onChange} rows="5" />
      </TextAreaWrapper>
    );
  }
}

TextArea.propTypes = {
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default TextArea;
