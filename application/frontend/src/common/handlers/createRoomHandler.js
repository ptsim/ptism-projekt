import RequestService from 'common/services/RequestService';

export const createNewRoom = newRoomData => RequestService.post(`/api/room/`, newRoomData);

export const checkIfNameExist = roomName => RequestService.get(`/api/room/${roomName}/exist`);
