export default {
  url: 'https://www.ptism.test/auth/',
  realm: 'doctord',
  clientId: 'frontend',
  onLoad: 'login-required',
};
