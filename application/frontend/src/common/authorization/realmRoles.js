const ROLE_ADMIN = 'admin';
const ROLE_PATIENT = 'app-patient';
const ROLE_DOCTOR = 'app-doctor';

export default {
  ROLE_ADMIN,
  ROLE_PATIENT,
  ROLE_DOCTOR,
};
