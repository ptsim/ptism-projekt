import { createNewRoom, checkIfNameExist } from 'common/handlers/createRoomHandler';

export const CREATE_ROOM_PENDING = 'CREATE_ROOM_PENDING';
export const CREATE_ROOM_OK = 'CREATE_ROOM_OK';
export const CREATE_ROOM_FAIL = 'CREATE_ROOM_FAIL';
export const CREATE_ROOM_DEFAULT = 'CREATE_ROOM_DEFAULT';

export const CHECK_ROOM_NAME_PENDING = 'CHECK_ROOM_NAME_PENDING';
export const CHECK_ROOM_NAME_OK = 'CHECK_ROOM_NAME_OK';
export const CHECK_ROOM_NAME_FAIL = 'CHECK_ROOM_NAME_FAIL';

export const GET_CATEGORIES_PENDING = 'GET_CATEGORIES_PENDING';
export const GET_CATEGORIES_OK = 'GET_CATEGORIES_OK';
export const GET_CATEGORIES_FAIL = 'GET_CATEGORIES_FAIL';

export const makeCreateRoomPending = () => ({
  type: CREATE_ROOM_PENDING,
});

export const makeCreateRoomOk = data => ({
  type: CREATE_ROOM_OK,
  payload: { data },
});

export const makeCreateRoomFail = () => ({
  type: CREATE_ROOM_FAIL,
});

export const makeGetCategoriesPending = () => ({
  type: GET_CATEGORIES_PENDING,
});

export const makeGetCategoriesOk = categories => ({
  type: GET_CATEGORIES_OK,
  payload: { categories },
});

export const makeGetCategoriesFail = () => ({
  type: GET_CATEGORIES_FAIL,
});

export const makeCheckRoomNamePending = () => ({
  type: CHECK_ROOM_NAME_PENDING,
});

export const makeCheckRoomNameOk = isTaken => ({
  type: CHECK_ROOM_NAME_OK,
  payload: { isTaken },
});

export const makeCheckRoomNameFail = () => ({
  type: CHECK_ROOM_NAME_FAIL,
});

export const makeCreateRoomDefault = () => ({
  type: CREATE_ROOM_DEFAULT,
});

export const createRoom = newRoomData => dispatch => {
  dispatch(makeCreateRoomPending());

  return createNewRoom(newRoomData)
    .then(res => {
      dispatch(makeCreateRoomOk(res.data));
    })
    .catch(() => {
      dispatch(makeCreateRoomFail());
    });
};

export const createRoomSetDefault = () => dispatch => {
  dispatch(makeCreateRoomDefault());
};

export const checkRoomName = roomName => dispatch => {
  dispatch(makeCheckRoomNamePending());

  return checkIfNameExist(roomName)
    .then(res => {
      dispatch(makeCheckRoomNameOk(res.data));
    })
    .catch(() => {
      dispatch(makeCheckRoomNameFail());
    });
};
