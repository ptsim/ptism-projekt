import {
  CREATE_ROOM_PENDING,
  CREATE_ROOM_OK,
  CREATE_ROOM_FAIL,
  CREATE_ROOM_DEFAULT,
  CHECK_ROOM_NAME_PENDING,
  CHECK_ROOM_NAME_OK,
  CHECK_ROOM_NAME_FAIL,
} from 'common/actions/createRoomActions';

export const CREATE_ROOM_INITIAL_STATE = {
  isError: false,
  isLoading: false,
  isSuccess: false,
  wasNameChecked: false,
  isNameLoading: false,
  nameTaken: false,
  categoriesList: [],
  createdRoomId: '',
};

export default (state, { type, payload }) => {
  const stateDefinition = typeof state === 'undefined' ? CREATE_ROOM_INITIAL_STATE : state;
  switch (type) {
    case CREATE_ROOM_OK:
      console.log(payload);
      return {
        ...stateDefinition,
        isError: false,
        isLoading: false,
        isSuccess: true,
        createdRoomId: payload.data.id,
      };
    case CREATE_ROOM_FAIL:
      return { ...stateDefinition, isLoading: false, isError: true };
    case CREATE_ROOM_PENDING:
      return { ...stateDefinition, isLoading: true, isError: false };
    case CREATE_ROOM_DEFAULT:
      return CREATE_ROOM_INITIAL_STATE;
    case CHECK_ROOM_NAME_PENDING:
      return {
        ...stateDefinition,
        isNameLoading: true,
        nameTaken: false,
        wasNameChecked: false,
        isError: false,
      };
    case CHECK_ROOM_NAME_OK:
      return {
        ...stateDefinition,
        isNameLoading: false,
        nameTaken: payload.isTaken,
        wasNameChecked: true,
      };
    case CHECK_ROOM_NAME_FAIL:
      return {
        ...stateDefinition,
        isNameLoading: false,
        nameTaken: false,
        wasNameChecked: true,
        isError: true,
      };
    default:
      return stateDefinition;
  }
};
