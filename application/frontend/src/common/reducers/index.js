import { combineReducers } from 'redux';
import lockScreenReducer from 'common/reducers/lockScreenReducer';
import authUserReducer from 'common/reducers/authUserReducer';
import createRoomReducer from 'common/reducers/createRoomReducer';
import newMessagesReducer from 'common/reducers/newMessagesReducer';

export default combineReducers({
  applicationScreen: lockScreenReducer,
  authUser: authUserReducer,
  createRoom: createRoomReducer,
  messagesSidebar: newMessagesReducer,
});
