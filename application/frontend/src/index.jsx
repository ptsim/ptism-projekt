import React from 'react';
import ReactDOM from 'react-dom';
import App from 'App';
import 'react-datepicker/dist/react-datepicker.css';
import 'react-notifications-component/dist/theme.css';

ReactDOM.render(<App />, document.getElementById('root'));
