import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import shortid from 'shortid';
import { connect } from 'react-redux';

import MessageBoxArea from 'messages-page/components/MessageBoxArea';
import MessagesListArea from 'messages-page/components/MessagesListArea';
import MessagePatientDetails from 'messages-page/components/MessagePatientDetails';
import { setCurrentChat } from 'messages-page/actions/newMessageActions';
import { fetchChatHistory, addNewUserToChatBox } from 'messages-page/actions/messagesListActions';

const MessagesPageWrapper = styled.div.attrs({ className: 'message-page-wrapper' })`
  width: calc(100% - 60px);
  height: calc(100vh - 81px - 60px);
  padding: 30px;
`;

const PageName = styled.h1.attrs({ className: 'page-name' })`
  font-weight: 100;
  font-family: 'Roboto-Thin';
  margin: 0 0 10px 0;
  height: 40px;
`;

const MessagesGrid = styled.div.attrs({ className: 'message-grid' })`
  display: grid;
  grid-template-columns: 2fr 8fr 3fr;
  box-shadow: 0 3px 8px rgba(0, 0, 0, 0.1);
  border: 1px solid #ddd;
  border-radius: 3px;
  height: calc(100% - 40px - 10px);
  width: 100%;
`;

class MessagesPage extends Component {
  addNewUser = username => {
    if (this.props.chatList.some(message => message.sender === username)) {
      this.props.setCurrentChatFunc(username);
      this.props.fetchChatHistoryFunc(username);
    } else {
      const newMessage = {
        id: shortid.generate(),
        sender: username,
        dateTime: new Date(),
        content: '',
        unread: 0,
        active: false,
      };

      this.props.addNewUserToChatBoxFunc(newMessage);
    }
  };

  render() {
    return (
      <MessagesPageWrapper>
        <PageName>Wiadomości</PageName>
        <MessagesGrid>
          <MessagesListArea addNewUser={this.addNewUser} />
          <MessageBoxArea addNewUser={this.addNewUser} />
          <MessagePatientDetails />
        </MessagesGrid>
      </MessagesPageWrapper>
    );
  }
}

MessagesPage.propTypes = {
  setCurrentChatFunc: PropTypes.func.isRequired,
  fetchChatHistoryFunc: PropTypes.func.isRequired,
  addNewUserToChatBoxFunc: PropTypes.func.isRequired,
  chatList: PropTypes.instanceOf(Array).isRequired,
};

const mapStateToProps = state => ({
  chatList: state.messages.messagesList.chatList,
});

const mapDispatchToProps = dispatch => ({
  setCurrentChatFunc: username => dispatch(setCurrentChat(username)),
  fetchChatHistoryFunc: username => dispatch(fetchChatHistory(username)),
  addNewUserToChatBoxFunc: chatBoxItem => dispatch(addNewUserToChatBox(chatBoxItem)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MessagesPage);
