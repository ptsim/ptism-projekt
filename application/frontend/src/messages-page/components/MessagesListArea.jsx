/* eslint-disable no-nested-ternary */
import React, { Component } from 'react';
import styled from 'styled-components';
import MessageSearchInput from 'messages-page/components/MessageSearchInput';
import MessageItem from 'messages-page/components/MessageItem';
import NewMessageModal from 'messages-page/components/NewMessageModal';
import { setCurrentChat } from 'messages-page/actions/newMessageActions';
import {
  fetchChatBoxList,
  fetchChatHistory,
  markChatWithUserAsRead,
} from 'messages-page/actions/messagesListActions';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import ProgressIndicatorCircular from 'common/components/ProgressIndicatorCircular';

const MessagesListAreaWrapper = styled.div.attrs({ className: 'messages-list-area-wrapper' })`
  grid: messages-list-area;
  background: #fff;
  border-right: 1px solid #f0f0f0;
  position: relative;
`;

const MessagesList = styled.div.attrs({ className: 'messages-list' })`
  margin-top: 12px;
  max-height: 95vh;
  overflow-x: hidden;
  overflow-y: auto;

  @media only screen and (max-width: 990px) {
    margin-top: 65px;
  }
`;

const MessageListHeader = styled.div.attrs({ className: 'message-list-header' })`
  display: block;
  width: 100%;
`;

const NoMessagesBox = styled.div.attrs({ className: 'no-messages-box' })`
  text-align: center;
  font-size: 10px;
  width: 60%;
  padding: 80px 20px;
  margin: 20px auto 0 auto;
  background: #fafafa;
  border: 1px solid #f0f0f0;
  border-radius: 4px;
  font-weight: 100;
`;

const ErrorBlock = styled.div.attrs({ className: 'error-block' })`
  padding: 20px 10px;
  margin: 5px 0;
  text-align: center;
  background: #fce7e6;
  font-size: 12px;
  color: #552526;
  width: 80%;
  margin: 20px auto;
`;

const NewMessageButton = styled.div.attrs({ className: 'new-message-button' })`
  width: 80%;
  margin: 0 auto;
  font-weight: 100;
  bottom: 20px;
  border-radius: 4px;
  text-align: center;
  cursor: pointer;
  background: #f0f0f0;
  border: 1px solid #f0f0f0;
  padding: 10px;
  transition: 0.2s;
  font-size: 10px;
  position: absolute;
  bottom: 20px;
  left: 9px;

  &:hover {
    opacity: 0.8;
  }
`;

class MessagesListArea extends Component {
  state = {
    shouldShowNewMessageModal: false,
  };

  componentWillMount() {
    this.props.fetchChatBoxListFunc();
  }

  setCurrentChatClick = message => {
    this.props.setCurrentChatFunc(message.sender);
    this.props.fetchChatHistoryFunc(message.sender);
    this.props.markChatWithUserAsReadFunc(message.sender);
  };

  showNewMessageModal = () => (
    <NewMessageModal
      onClose={() => {
        this.setState({ shouldShowNewMessageModal: false });
      }}
      onChoice={username => {
        this.props.addNewUser(username);

        this.setState({
          shouldShowNewMessageModal: false,
        });
      }}
    />
  );

  clickModal = () => {
    this.setState({
      shouldShowNewMessageModal: !this.state.shouldShowNewMessageModal,
    });
  };

  renderMessages = () => {
    const valueToMap =
      this.props.searchValue.length > 0 ? this.props.filteredChatList : this.props.chatList;
    return (
      <MessagesList>
        {valueToMap.map(message => (
          <MessageItem
            key={message.id}
            messageObject={message}
            onClick={() => this.setCurrentChatClick(message)}
            chosenChatUser={this.props.chosenChatUser}
          />
        ))}
      </MessagesList>
    );
  };

  render() {
    const { shouldShowNewMessageModal } = this.state;
    const { isChatListLoading, isChatListError } = this.props;
    const valueToMap =
      this.props.searchValue.length > 0 ? this.props.filteredChatList : this.props.chatList;

    return (
      <MessagesListAreaWrapper>
        <MessageListHeader>
          <MessageSearchInput />
        </MessageListHeader>
        {isChatListLoading ? (
          <ProgressIndicatorCircular size={40} />
        ) : isChatListError ? (
          <ErrorBlock>Wystąpił błąd podczas pobierania listy wiadomości.</ErrorBlock>
        ) : valueToMap.length === 0 ? (
          <NoMessagesBox>Brak nowych wiadomości.</NoMessagesBox>
        ) : (
          this.renderMessages()
        )}
        {shouldShowNewMessageModal && this.showNewMessageModal()}
        <NewMessageButton onClick={this.clickModal}>Nowa wiadomość</NewMessageButton>
      </MessagesListAreaWrapper>
    );
  }
}

MessagesListArea.propTypes = {
  setCurrentChatFunc: PropTypes.func.isRequired,
  chosenChatUser: PropTypes.string.isRequired,
  searchValue: PropTypes.string.isRequired,
  addNewUser: PropTypes.func.isRequired,
  fetchChatHistoryFunc: PropTypes.func.isRequired,
  fetchChatBoxListFunc: PropTypes.func.isRequired,
  markChatWithUserAsReadFunc: PropTypes.func.isRequired,
  isChatListLoading: PropTypes.bool.isRequired,
  isChatListError: PropTypes.bool.isRequired,
  chatList: PropTypes.instanceOf(Array).isRequired,
  filteredChatList: PropTypes.instanceOf(Array).isRequired,
};

const mapStateToProps = state => ({
  chosenChatUser: state.messages.usersList.currentChatUsername,
  isChatListLoading: state.messages.messagesList.isChatListLoading,
  isChatListError: state.messages.messagesList.isChatListError,
  chatList: state.messages.messagesList.chatList,
  filteredChatList: state.messages.messagesList.filteredChatList,
  searchValue: state.messages.messagesList.searchValue,
});

const mapDispatchToProps = dispatch => ({
  setCurrentChatFunc: username => dispatch(setCurrentChat(username)),
  fetchChatBoxListFunc: () => dispatch(fetchChatBoxList()),
  fetchChatHistoryFunc: username => dispatch(fetchChatHistory(username)),
  markChatWithUserAsReadFunc: username => dispatch(markChatWithUserAsRead(username)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MessagesListArea);
