import RequestService from 'common/services/RequestService';

export const getChatBoxList = () => RequestService.get(`/api/chat/messages`);

export const getChatHistory = username =>
  RequestService.get(`/api/chat/history?username=${username}`);

export const markChatAsRead = username => RequestService.get(`/api/chat/read?username=${username}`);
