import React from 'react';
import styled from 'styled-components';

import WelcomeBackBadge from 'main-page/components/WelcomeBackBadge';
import DashboardStats from 'main-page/components/DashboardStats';
import DashboardGoToPages from 'main-page/components/DashboardGoToPages';
import DashboardChart from 'main-page/components/DashboardChart';
import UserInfo from 'main-page/components/UserInfo';

const MainPageWrapper = styled.div.attrs({ className: 'main-page-wrapper' })`
  display: grid;
  grid-template-columns: 22fr 8fr;
  padding: 30px;
  height: calc(100% - 60px);
  width: calc(100% - 60px);
`;

const DashboardLeft = styled.div.attrs({ className: 'dashboard-left' })`
  padding: 0 20px;
`;

const DashboardRight = styled.div.attrs({ className: 'dashboard-right' })``;

const MainPage = () => (
  <MainPageWrapper>
    <DashboardLeft>
      <WelcomeBackBadge />
      <DashboardStats />
      <DashboardGoToPages />
      <DashboardChart />
    </DashboardLeft>
    <DashboardRight>
      <UserInfo />
    </DashboardRight>
  </MainPageWrapper>
);

export default MainPage;
