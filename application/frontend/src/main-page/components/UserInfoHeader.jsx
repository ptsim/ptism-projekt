import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import RealmRolesService from 'common/authorization/RealmRolesService';
import realmRoles from 'common/authorization/realmRoles';

const UserInfoHeaderWrapper = styled.div.attrs({ className: 'user-info-header-wrapper' })`
  display: flex;
  padding-bottom: 20px;
  border-bottom: 1px solid #ccc;
`;

const UserInfoImageWrapper = styled.div.attrs({ className: 'user-info-image-wrapper' })``;

const UserInfoTextWrapper = styled.div.attrs({ className: 'user-info-text-wrapper' })``;

const UserInfoImage = styled.div.attrs({ className: 'user-info-image' })`
  width: 50px;
  height: 50px;
  border-radius: 50%;
  border: 2px solid #f4f4f4;
  background: #2d4564;
  line-height: 50px;
  color: #fff;
  text-align: center;
  font-size: 18px;
  transition: 0.2s;
  cursor: pointer;
  margin-right: 10px;

  &:hover {
    opacity: 0.85;
  }
`;

const UserName = styled.p.attrs({ className: 'user-name' })`
  font-weight: 500;
  font-size: 16px;
  margin: 0;
  line-height: 40px;
`;

const UserStatus = styled.p.attrs({ className: 'user-status' })`
  font-size: 12px;
  font-weight: 200;
  margin: 0;
  line-height: 5px;
`;

const UserInfoHeader = ({ username }) => {
  const getCircleText = () => username.charAt(0).toUpperCase();

  const isDoctor = () => RealmRolesService.hasRole(realmRoles.ROLE_DOCTOR);

  const getRoleTitle = () => (isDoctor ? 'Doktor' : 'Pacjent');

  return (
    <UserInfoHeaderWrapper>
      <UserInfoImageWrapper>
        <UserInfoImage>{getCircleText()}</UserInfoImage>
      </UserInfoImageWrapper>
      <UserInfoTextWrapper>
        <UserName>{username}</UserName>
        <UserStatus>{getRoleTitle()}</UserStatus>
      </UserInfoTextWrapper>
    </UserInfoHeaderWrapper>
  );
};

UserInfoHeader.propTypes = {
  username: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  username: state.common.authUser.keycloakInfo.userInfo.preferred_username,
});

export default connect(mapStateToProps, null)(UserInfoHeader);
