import React from 'react';
import styled from 'styled-components';

import appointmentIcon from 'images/icons/dashboard/appointment.svg';
import doctorIcon from 'images/icons/dashboard/doctor.svg';
import waitIcon from 'images/icons/dashboard/wait.svg';
import queueIcon from 'images/icons/dashboard/queue.svg';

const DashboardStatsWrapper = styled.div.attrs({ className: 'dashboard-stats-wrapper' })`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  gap: 20px;
  margin: 20px 0;
`;

const StatItem = styled.div.attrs({ className: 'stat-item' })`
  display: flex;
  padding: 20px;
  background: #ffffff;
  border: 1px solid #ddd;
  box-shadow: 0 3px 8px rgba(0, 0, 0, 0.1);
  transition: 0.2s;

  &:hover {
    transform: scale(1.02);
    opacity: 0.9;
  }
`;

const StatImageWrapper = styled.div.attrs({ className: 'stat-image-wrapper' })`
  padding: 10px;
  border-radius: 50%;
  background: rgba(45, 69, 100, 0.9);
  margin-right: 10px;
  display: flex;
  height: 20px;
`;

const StatImage = styled.img.attrs({ className: 'stat-image' })`
  width: 16px;
  height: 16px;
  filter: invert(1);
`;

const StatDescription = styled.div.attrs({ className: 'stat-description' })`
  font-size: 12px;
  line-height: 20px;
`;

const Count = styled.p.attrs({ className: 'count' })`
  margin: 0;
  padding: 0;
  font-weight: 500;
  font-size: 16px;
`;

const Name = styled.p.attrs({ className: 'name' })`
  margin: 0;
  padding: 0;
  font-weight: 200;
`;

const STAT_ITEMS = [
  {
    id: 1,
    name: 'Odbyte wizyty',
    count: 123,
    icon: appointmentIcon,
  },
  {
    id: 2,
    name: 'Lekarzy',
    count: 123,
    icon: doctorIcon,
  },
  {
    id: 3,
    name: 'Oczekiwanie',
    count: 123,
    icon: waitIcon,
  },
  {
    id: 4,
    name: 'W kolejce',
    count: 123,
    icon: queueIcon,
  },
];

const DashboardStats = () => (
  <DashboardStatsWrapper>
    {STAT_ITEMS.map(item => (
      <StatItem key={item.id}>
        <StatImageWrapper>
          <StatImage src={item.icon} />
        </StatImageWrapper>
        <StatDescription>
          <Count>{item.count}</Count>
          <Name>{item.name}</Name>
        </StatDescription>
      </StatItem>
    ))}
  </DashboardStatsWrapper>
);

export default DashboardStats;
