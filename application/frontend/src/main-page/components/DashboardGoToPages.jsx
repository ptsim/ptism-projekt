import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import visitIcon from 'images/icons/dashboard/calendar.svg';
import historyIcon from 'images/icons/dashboard/history.svg';
import arrowIcon from 'images/icons/dashboard/down-arrow.svg';

const DashboardGoToPagesWrapper = styled.div.attrs({ className: 'dashboard-go-to-pages-wrapper' })`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 20px;
`;

const GoToItem = styled(Link).attrs({ className: 'go-to-item' })`
  transition: 0.2s;
  background: #ffffff;
  border: 1px solid #ddd;
  text-decoration: none;
  color: #000;
  padding: 20px;
  text-align: center;
  box-shadow: 0 3px 8px rgba(0, 0, 0, 0.1);

  &:hover {
    background: rgba(45, 69, 100, 0.9);
    color: #fff;

    .arrow-icon {
      transform: translate(0, 55%);
    }

    img {
      filter: invert(1);
    }
  }
`;

const ItemIcon = styled.img.attrs({ className: 'item-icon' })`
  width: 40px;
`;

const ArrowIcon = styled.img.attrs({ className: 'arrow-icon' })`
  width: 15px;
  transition: 0.2s;
  margin-top: 20px;
`;

const ItemName = styled.div.attrs({ className: 'item-name' })`
  font-weight: 600;
  text-transform: uppercase;
  font-size: 12px;
  margin: 10px 0;
`;

const ItemDescription = styled.div.attrs({ className: 'item-description' })`
  font-size: 15px;
  font-weight: 200;
  width: 60%;
  margin: 0 auto;
`;

const LINK_ITEMS = [
  {
    id: 1,
    name: 'Wizyta',
    description: 'Kliknij tutaj, aby się zarejestrować.',
    to: '/visit',
    icon: visitIcon,
  },
  {
    id: 2,
    name: 'Historia',
    description: 'Kliknij tutaj, aby zobaczyć historię swoich wizyt',
    to: '/history',
    icon: historyIcon,
  },
];

const DashboardGoToPages = () => (
  <DashboardGoToPagesWrapper>
    {LINK_ITEMS.map(link => (
      <GoToItem key={link.id} to={link.to}>
        <ItemIcon src={link.icon} />
        <ItemName>{link.name}</ItemName>
        <ItemDescription>{link.description}</ItemDescription>
        <ArrowIcon src={arrowIcon} />
      </GoToItem>
    ))}
  </DashboardGoToPagesWrapper>
);

export default DashboardGoToPages;
