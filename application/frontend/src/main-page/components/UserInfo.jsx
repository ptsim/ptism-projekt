import React from 'react';
import styled from 'styled-components';

import UserInfoHeader from 'main-page/components/UserInfoHeader';
import SmallCalendar from 'main-page/components/SmallCalendar';

const UserInfoWrapper = styled.div.attrs({ className: 'user-info-wrapper' })`
  background: #fff;
  border: 1px solid #ddd;
  padding: 30px;
`;

const UserInfo = () => (
  <UserInfoWrapper>
    <UserInfoHeader firstName="John" lastName="Doe" />
    <SmallCalendar />
  </UserInfoWrapper>
);

export default UserInfo;
