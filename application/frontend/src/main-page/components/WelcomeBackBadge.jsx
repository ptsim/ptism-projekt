/* eslint-disable prefer-template */
import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import badgeBg from 'images/gradient_2.svg';
import doctorImg from 'images/doctor-woman.svg';

const NewUserBadgeWrapper = styled.div.attrs({ className: 'new-user-badge-wrapper' })`
  background: url(${badgeBg});
  background-size: cover;
  border-radius: 0px;
  width: calc(100% - 100px);
  color: #fff;
  font-family: 'Roboto-Light', sans-serif;
  padding: 20px 50px;
  position: relative;
`;

const StudyImage = styled.img.attrs({ className: 'study-image', alt: 'study-image' })`
  height: 130px;
  position: absolute;
  right: 15px;
  top: -20px;
`;

const BadgeHeader = styled.h2.attrs({ className: 'badge-header' })`
  margin: 0;
  font-size: 18px;
  font-family: 'Roboto-Light', sans-serif;
  font-weight: 600;

  @media only screen and (max-width: 600px) {
    font-size: 15px;
  }
`;

const BadgeDescription = styled.p.attrs({ className: 'badge-description' })`
  font-size: 12px;
  font-weight: 300;
  width: 50%;

  @media only screen and (max-width: 600px) {
    font-size: 10px;
  }
`;

const BadgeBottomText = styled.p.attrs({ className: 'badge-bottom-text' })`
  font-size: 10px;
  font-weight: 300;
  width: 70%;
  margin-top: 0px;
`;

const BoldText = styled.span.attrs({ className: 'bold-text' })`
  font-weight: bold;
`;

const monthNames = [
  'Styczeń',
  'Luty',
  'Marzec',
  'Kwiecień',
  'Maj',
  'Czerwiec',
  'Lipiec',
  'Sierpień',
  'Wrzesień',
  'Październik',
  'Listopad',
  'Grudzień',
];

const dayNames = ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'];

const WelcomeBackBadge = ({ username }) => {
  const addZeroBefore = n => (n < 10 ? '0' : '') + n;

  const renderTodayDate = () => {
    const date = new Date();

    return (
      addZeroBefore(date.getDate()) +
      ' ' +
      monthNames[date.getMonth()] +
      ' ' +
      addZeroBefore(date.getFullYear()) +
      ', ' +
      dayNames[date.getDay()]
    );
  };

  return (
    <NewUserBadgeWrapper>
      <BadgeHeader>Witaj spowrotem, {username}!</BadgeHeader>
      <BadgeDescription>
        Odbyłeś już <BoldText>10 wizyt</BoldText> za pomocą naszej aplikacji, oby tak dalej!
      </BadgeDescription>
      <BadgeBottomText>
        Dzisiaj: <BoldText>{renderTodayDate()}</BoldText>
      </BadgeBottomText>
      <StudyImage src={doctorImg} />
    </NewUserBadgeWrapper>
  );
};

WelcomeBackBadge.propTypes = {
  username: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  username: state.common.authUser.keycloakInfo.userInfo.preferred_username,
});

export default connect(mapStateToProps, null)(WelcomeBackBadge);
