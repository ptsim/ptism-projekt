export const isText = text => /^.{3,}$/.test(text);

export const isPassword = password => /^.{3,}$/.test(password);

export const isEmail = email =>
  /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(
    email,
  );

export const isPhoneNumber = number => /^-{0,1}\d+$/.test(number);

export const isPostalCode = postalCode => /^([0-9]{2})(-[0-9]{3})?$/.test(postalCode);
