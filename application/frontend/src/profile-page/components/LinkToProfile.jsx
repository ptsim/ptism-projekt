/* eslint-disable no-return-assign */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import copyIcon from 'images/icons/copy.svg';
import { CopyToClipboard } from 'react-copy-to-clipboard';

const LinkToProfileWrapper = styled.div.attrs({ className: 'link-to-profile-wrapper' })`
  margin-top: 15px;
  position: relative;
  width: 100%;
  margin-bottom: 20px;
`;

const CopyButton = styled.button.attrs({ className: 'copy-button' })`
  position: absolute;
  right: 18px;
  top: 0px;
  border: none;
  border-left: 1px solid #f0f0f0;
  cursor: pointer;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  background: none;
  padding: 10px 10px 9px 10px;
  outline: none;
  transition: 0.2s;

  &:hover {
    background: #f0f0f0;
  }
`;

const CopyIcon = styled.img.attrs({ className: 'copy-icon', alt: 'copy-icon' })`
  width: 16px;
  height: 16px;
`;

const LinkComponent = styled.input.attrs({ className: 'link-input-component' })`
  background: none;
  border-radius: 4px;
  border: 1px solid #f0f0f0;
  padding: 10px 45px 10px 15px;
  font-size: 12px;
  margin: 0 auto;
  display: block;
`;

class LinkToProfile extends Component {
  copyToClipboard = event => {
    this.inputComp.select();
    document.execCommand('copy');
    event.target.focus();
  };

  render() {
    return (
      <LinkToProfileWrapper>
        <LinkComponent value={this.props.link} disabled ref={input => (this.inputComp = input)} />

        <CopyToClipboard text={this.props.link}>
          <CopyButton>
            <CopyIcon src={copyIcon} onClick={this.copyToClipboard} />
          </CopyButton>
        </CopyToClipboard>
      </LinkToProfileWrapper>
    );
  }
}

LinkToProfile.propTypes = {
  link: PropTypes.string.isRequired,
};

export default LinkToProfile;
