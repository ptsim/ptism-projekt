import React from 'react';
import styled from 'styled-components';
import ToggleSwitch from 'common/components/ToggleSwitch';
import UpdateButton from 'profile-page/components/UpdateButton';

const ProfileNotificationsWrapper = styled.div.attrs({
  className: 'profile-notifications-wrapper',
})``;

const ProfileGridWrapper = styled.div.attrs({ className: 'profile-grid-wrapper' })`
  padding: 0 20px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-template-areas: 'profile-form-left profile-form-right';
`;

const ProfileFormLeft = styled.div.attrs({ className: 'profile-form-left' })`
  grid: profile-form-left;
`;

const ProfileFormRight = styled.div.attrs({ className: 'profile-form-right' })`
  grid: profile-form-right;
`;

const PrivacyOption = styled.p.attrs({ className: 'privacy options' })`
  font-size: 13px;
  color: #59637d;
  margin: 0;
  padding: 30px 0;
  border-bottom: 1px solid #f0f0f0;

  &:last-child {
    border-bottom: none;
  }
`;

const ProfileNotifications = () => (
  <ProfileNotificationsWrapper>
    <ProfileGridWrapper>
      <ProfileFormLeft>
        <PrivacyOption>Włącz dźwięki powiadomień</PrivacyOption>
        <PrivacyOption>Wysyłaj email po otrzymaniu wiadomości prywatnej</PrivacyOption>
        <PrivacyOption>Wysyłaj email przy kazdym powiadomieniu</PrivacyOption>
      </ProfileFormLeft>
      <ProfileFormRight>
        <PrivacyOption>
          <ToggleSwitch />
        </PrivacyOption>
        <PrivacyOption>
          <ToggleSwitch />
        </PrivacyOption>
        <PrivacyOption>
          <ToggleSwitch />
        </PrivacyOption>
      </ProfileFormRight>
    </ProfileGridWrapper>

    <UpdateButton style={{ marginTop: '10px' }} />
  </ProfileNotificationsWrapper>
);

export default ProfileNotifications;
