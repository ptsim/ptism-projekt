import React from 'react';
import styled from 'styled-components';
import ProfileHeader from 'profile-page/components/ProfileHeader';
import ProfileSettings from 'profile-page/components/ProfileSettings';

const ProfilePageWrapper = styled.div.attrs({ className: 'profile-page-wrapper' })`
  overflow: hidden;
`;

const ProfilePage = () => (
  <ProfilePageWrapper>
    <ProfileHeader />
    <ProfileSettings />
  </ProfilePageWrapper>
);

export default ProfilePage;
