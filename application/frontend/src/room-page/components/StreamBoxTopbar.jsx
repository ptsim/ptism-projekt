import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Colors from 'common/colors';
import { removeRoom } from 'room-page/actions/roomDetailsActions';
import DeleteRoomDialog from 'room-page/components/DeleteRoomDialog';

const StreamBoxTopbarWrapper = styled.div.attrs({ className: 'stream-box-topbar' })`
  display: grid;
  grid-template-columns: 1fr 3fr;
  grid-template-areas:
    'bar-left-area'
    'bar-right-area';

  height: 100%;
  background: ${Colors.WHITE};
  padding: 20px;
  border-bottom: 1px solid ${Colors.GALLERY};
  height: 14px;
  line-height: 14px;
`;

const TopBarLeft = styled.div.attrs({ className: 'top-bar-left' })`
  grid: bar-left-area;
  font-size: 14px;
  font-weight: bold;
  font-family: 'Titillium Web', sans-serif;
`;

const TopBarRight = styled.div.attrs({ className: 'top-bar-right' })`
  grid: bar-right-area;
  text-align: right;
`;

const DeleteRoomButton = styled.button.attrs({ className: 'delete-room-button' })`
  cursor: pointer;
  transition: 0.2s;
  padding: 10px 20px;
  color: #552526;
  border: 1px solid #f0f0f0;
  border-radius: 4px;
  background: #fce7e6;
  font-weight: bold;
  margin: -10px 20px 0 0;

  &:hover {
    opacity: 0.7;
  }

  @media only screen and (max-width: 880px) {
    display: none;
  }
`;

const BoldText = styled.span.attrs({ className: 'bold-text' })`
  font-weight: bold;
  font-family: 'Sen', sans-serif;
`;
class StreamBoxTopbar extends Component {
  state = {
    showDeleteDialog: false,
  };

  onDeleteClick = () => {
    this.setState({
      showDeleteDialog: !this.state.showDeleteDialog,
    });
  };

  showDeleteButton = () => this.props.username === this.props.owner;

  render() {
    const { roomName, removeRoomFunc, roomId } = this.props;
    const { showDeleteDialog } = this.state;
    return (
      <StreamBoxTopbarWrapper>
        <TopBarLeft>
          Nazwa pokoju: <BoldText>{roomName}</BoldText>
        </TopBarLeft>
        <TopBarRight>
          {this.showDeleteButton() ? (
            <DeleteRoomButton onClick={this.onDeleteClick}>Usuń pokój</DeleteRoomButton>
          ) : null}
        </TopBarRight>
        {showDeleteDialog && (
          <DeleteRoomDialog
            onClose={this.onDeleteClick}
            onDelete={removeRoomFunc}
            roomId={roomId}
            roomName={roomName}
          />
        )}
      </StreamBoxTopbarWrapper>
    );
  }
}

StreamBoxTopbar.propTypes = {
  roomName: PropTypes.string.isRequired,
  roomId: PropTypes.string.isRequired,
  owner: PropTypes.string.isRequired,
  username: PropTypes.string.isRequired,
  removeRoomFunc: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  roomName: state.session.roomDetails.roomData.name,
  roomId: state.session.roomDetails.roomId,
  owner: state.session.roomDetails.roomData.owner,
  username: state.common.authUser.keycloakInfo.userInfo.preferred_username,
});

const mapDispatchToProps = dispatch => ({
  removeRoomFunc: roomId => dispatch(removeRoom(roomId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(StreamBoxTopbar);
