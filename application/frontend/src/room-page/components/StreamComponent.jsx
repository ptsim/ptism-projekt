import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import DetailsBox from 'room-page/components/DetailsBox';
import UserVideoComponent from 'room-page/components/UserVideoComponent';
import shortid from 'shortid';
import ProgressIndicatorCircular from 'common/components/ProgressIndicatorCircular';
import InitialSettings from 'room-page/components/InitialSettings';

const VideoComponentWrapper = styled.div.attrs({ className: 'video-component-wrapper' })`
  display: inline-block;
  font-family: 'Sen', sans-serif;
`;

const StreamComponentWrapper = styled.div.attrs({ className: 'stream-component-wrapper' })`
  overflow: hidden;
  max-height: 83vh;
  width: 100%;
  max-width: 100%;
  display: grid;
  grid-gap: 0;
  grid-template-rows: 22fr 2fr;
  grid-template-areas:
    'stream-box-area'
    'details-box-area';
  height: 100%;

  @media only screen and (max-height: 770px) {
    max-height: 82vh;
  }

  @media only screen and (max-height: 715px) {
    max-height: 81vh;
  }

  @media only screen and (max-height: 670px) {
    max-height: 80vh;
  }
`;

const VideoGrid = styled.div.attrs({ className: 'video-grid' })``;
const StreamBox = styled.div.attrs({ className: 'stream-box-area' })`
  grid: stream-box-area;
  max-height: 70vh;
`;

const MainVideoArea = styled.div.attrs({ className: 'main-video' })``;
const SmallVideoArea = styled.div.attrs({ className: 'small-video-area' })``;

const StreamComponent = props => (
  <StreamComponentWrapper id="session">
    {props.publisher === undefined ? (
      <InitialSettings
        onCheckboxChange={props.onCheckboxChange}
        initialAudio={props.initialAudio}
        initialVideo={props.initialVideo}
        joinSession={props.joinSession}
      />
    ) : null}
    <StreamBox id="main-video" className={props.fullScreenMode ? 'full-sc-video' : 'not-full'}>
      {!props.publisher && props.sessionStarted ? (
        <ProgressIndicatorCircular />
      ) : (
        <VideoGrid>
          <MainVideoArea>
            {props.mainStreamManager !== undefined ? (
              <UserVideoComponent
                isSharingScreen={props.isSharingScreen}
                isSpeaking={props.isSpeaking}
                bigVideo
                hiddenCamera={!props.publishVideo}
                streamManager={props.mainStreamManager}
              />
            ) : null}
          </MainVideoArea>
          <SmallVideoArea id="stream-container">
            {props.publisher !== undefined ? (
              <VideoComponentWrapper
                className="stream-container small-video"
                onClick={() => props.handleMainVideoStream(props.publisher)}
              >
                <UserVideoComponent
                  bigVideo={false}
                  hiddenCamera={!props.publishVideo}
                  streamManager={props.publisher}
                />
              </VideoComponentWrapper>
            ) : null}
            {props.subscribers.map(sub => (
              <VideoComponentWrapper
                key={shortid.generate()}
                className="stream-container"
                onClick={() => props.handleMainVideoStream(sub)}
              >
                <UserVideoComponent
                  bigVideo={false}
                  hiddenCamera={!props.publishVideo}
                  streamManager={sub}
                />
              </VideoComponentWrapper>
            ))}
          </SmallVideoArea>
        </VideoGrid>
      )}
    </StreamBox>

    <DetailsBox
      isSharingScreen={props.isSharingScreen}
      style={props.publisher !== undefined ? {} : { visibility: 'hidden' }}
      shareScreen={props.shareScreen}
      stopSharingScreen={props.stopSharingScreen}
      goFullScreen={props.goFullScreen}
      showVideo={props.showVideo}
      muteAudio={props.muteAudio}
      publishVideo={props.publishVideo}
      publishAudio={props.publishAudio}
    />
  </StreamComponentWrapper>
);

StreamComponent.propTypes = {
  onCheckboxChange: PropTypes.func.isRequired,
  joinSession: PropTypes.func.isRequired,
  stopSharingScreen: PropTypes.func.isRequired,
  goFullScreen: PropTypes.func.isRequired,
  shareScreen: PropTypes.func.isRequired,
  showVideo: PropTypes.func.isRequired,
  muteAudio: PropTypes.func.isRequired,
  handleMainVideoStream: PropTypes.func.isRequired,
  initialAudio: PropTypes.bool.isRequired,
  initialVideo: PropTypes.bool.isRequired,
  sessionStarted: PropTypes.bool.isRequired,
  isSharingScreen: PropTypes.bool.isRequired,
  isSpeaking: PropTypes.bool.isRequired,
  publishVideo: PropTypes.bool.isRequired,
  publishAudio: PropTypes.bool.isRequired,
  fullScreenMode: PropTypes.bool.isRequired,
  publisher: PropTypes.instanceOf(Object).isRequired,
  mainStreamManager: PropTypes.instanceOf(Object).isRequired,
  subscribers: PropTypes.instanceOf(Array).isRequired,
};

export default StreamComponent;
