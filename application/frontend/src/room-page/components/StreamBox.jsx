import React from 'react';
import styled from 'styled-components';
import StreamBoxTopbar from 'room-page/components/StreamBoxTopbar';
import StreamComponent from 'room-page/components/StreamComponent';
import PropTypes from 'prop-types';

const StreamBoxWrapper = styled.div.attrs({ className: 'details-box-wrapper' })`
  height: 100%;
`;

const StreamBox = props => (
  <StreamBoxWrapper>
    <StreamBoxTopbar />
    <StreamComponent
      onCheckboxChange={props.onCheckboxChange}
      joinSession={props.joinSession}
      stopSharingScreen={props.stopSharingScreen}
      goFullScreen={props.goFullScreen}
      shareScreen={props.shareScreen}
      showVideo={props.showVideo}
      muteAudio={props.muteAudio}
      getVideoClassName={props.getVideoClassName}
      handleMainVideoStream={props.handleMainVideoStream}
      initialAudio={props.initialAudio}
      initialVideo={props.initialVideo}
      sessionStarted={props.sessionStarted}
      isSharingScreen={props.isSharingScreen}
      publishVideo={props.publishVideo}
      isSpeaking={props.isSpeaking}
      publishAudio={props.publishAudio}
      publisher={props.publisher}
      mainStreamManager={props.mainStreamManager}
      subscribers={props.subscribers}
      fullScreenMode={props.fullScreenMode}
    />
  </StreamBoxWrapper>
);

StreamBox.propTypes = {
  onCheckboxChange: PropTypes.func.isRequired,
  joinSession: PropTypes.func.isRequired,
  stopSharingScreen: PropTypes.func.isRequired,
  goFullScreen: PropTypes.func.isRequired,
  shareScreen: PropTypes.func.isRequired,
  showVideo: PropTypes.func.isRequired,
  muteAudio: PropTypes.func.isRequired,
  getVideoClassName: PropTypes.func.isRequired,
  handleMainVideoStream: PropTypes.func.isRequired,
  initialAudio: PropTypes.bool.isRequired,
  initialVideo: PropTypes.bool.isRequired,
  sessionStarted: PropTypes.bool.isRequired,
  isSharingScreen: PropTypes.bool.isRequired,
  isSpeaking: PropTypes.bool.isRequired,
  publishVideo: PropTypes.bool.isRequired,
  publishAudio: PropTypes.bool.isRequired,
  fullScreenMode: PropTypes.bool.isRequired,
  publisher: PropTypes.instanceOf(Object).isRequired,
  mainStreamManager: PropTypes.instanceOf(Object).isRequired,
  subscribers: PropTypes.instanceOf(Array).isRequired,
};

export default StreamBox;
