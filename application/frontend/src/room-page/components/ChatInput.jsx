import React, { Component } from 'react';
import styled from 'styled-components';
import commentIcon from 'images/icons/comment.svg';
import PropTypes from 'prop-types';

const ChatInputWrapper = styled.div.attrs({ className: 'chat-input-wrapper' })`
  width: 74%;
  position: absolute;
  bottom: 0;
  margin: 0;
`;

const ChatInputElement = styled.input.attrs({ className: 'chat-input-wrapper' })`
  width: 100%;
  padding: 20px 60px 21px 20px;
  outline: none;
  border: 1px solid #f0f0f0;
  border-left: none;
`;

const InputWrapper = styled.div.attrs({ className: 'input-wrapper' })``;

const ChatInputIcon = styled.img.attrs({ className: 'chat-input-icon', alt: 'chat-input-icon' })`
  position: absolute;
  right: -60px;
  top: 20px;
  width: 24px;
  height: 24px;
  cursor: pointer;
`;

const PLACEHOLDER = 'Quick message...';

class ChatInput extends Component {
  state = {
    inputText: '',
  };

  onChange = event => {
    const {
      target: { value },
    } = event;

    this.setState({
      inputText: value,
    });
  };

  onEnter = event => {
    if (event.key === 'Enter') this.sendMessage();
  };

  sendMessage = () => {
    const messageObj = this.state.inputText;
    if (messageObj.length > 0) {
      this.props.onMessageSend(messageObj);
      this.setState({ inputText: '' });
    }
  };

  render() {
    return (
      <ChatInputWrapper>
        {this.props.isSession && (
          <InputWrapper>
            <ChatInputElement
              autoComplete="off"
              onChange={this.onChange}
              name="inputText"
              value={this.state.inputText}
              placeholder={PLACEHOLDER}
              onKeyDown={this.onEnter}
            />
            <ChatInputIcon src={commentIcon} onClick={this.sendMessage} />
          </InputWrapper>
        )}
      </ChatInputWrapper>
    );
  }
}

ChatInput.propTypes = {
  onMessageSend: PropTypes.func.isRequired,
  isSession: PropTypes.bool.isRequired,
};

export default ChatInput;
