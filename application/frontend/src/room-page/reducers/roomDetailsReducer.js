import {
  CONNECT_TO_ROOM_PENDING,
  CONNECT_TO_ROOM_OK,
  CONNECT_TO_ROOM_FAIL,
  LOGIN_TO_ROOM_PENDING,
  LOGIN_TO_ROOM_OK,
  LOGIN_TO_ROOM_FAIL,
  REMOVE_ROOM_PENDING,
  REMOVE_ROOM_OK,
  REMOVE_ROOM_FAIL,
  DEFAULT_REMOVE_ROOM_STATE,
} from 'room-page/actions/roomDetailsActions';

const INITIAL_STATE = {
  isLoading: false,
  isError: false,
  roomData: [],
  roomId: '',
  isPublic: false,
  isLoginError: false,
  isLoginPending: false,
  isRemoveLoading: false,
  isRemoveError: false,
  isRemoveSuccess: false,
};

export default (state, { type, payload }) => {
  const stateDefinition = typeof state === 'undefined' ? INITIAL_STATE : state;
  switch (type) {
    case CONNECT_TO_ROOM_PENDING:
      return { ...stateDefinition, isLoading: true, isError: false, roomData: [] };
    case CONNECT_TO_ROOM_OK:
      return {
        ...stateDefinition,
        isLoading: false,
        isError: false,
        roomData: payload.roomData,
        roomId: payload.roomData.id,
        isPublic: payload.roomData.is_public,
      };
    case CONNECT_TO_ROOM_FAIL:
      return { ...stateDefinition, isLoading: false, isError: true, roomData: [] };
    case LOGIN_TO_ROOM_PENDING:
      return { ...stateDefinition, isLoginError: false, isLoginPending: true };
    case LOGIN_TO_ROOM_OK:
      return { ...stateDefinition, isLoginError: false, isLoginPending: false, isPublic: true };
    case LOGIN_TO_ROOM_FAIL:
      return { ...stateDefinition, isLoginError: true, isLoginPending: false };
    case REMOVE_ROOM_PENDING:
      return {
        ...stateDefinition,
        isRemoveLoading: true,
        isRemoveError: false,
        isRemoveSuccess: false,
      };
    case REMOVE_ROOM_OK:
      return {
        ...stateDefinition,
        isRemoveLoading: false,
        isRemoveError: false,
        isRemoveSuccess: true,
      };
    case REMOVE_ROOM_FAIL:
      return {
        ...stateDefinition,
        isRemoveLoading: false,
        isRemoveError: true,
        isRemoveSuccess: false,
      };
    case DEFAULT_REMOVE_ROOM_STATE:
      return {
        ...stateDefinition,
        isRemoveLoading: false,
        isRemoveError: false,
        isRemoveSuccess: false,
      };
    default:
      return stateDefinition;
  }
};
