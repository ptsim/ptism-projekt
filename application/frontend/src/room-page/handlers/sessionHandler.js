import RequestService from 'common/services/RequestService';

export const getToken = sessionName => RequestService.post(`/session/join`, sessionName);

export const leaveSession = data => RequestService.post(`/session/leave`, data);
