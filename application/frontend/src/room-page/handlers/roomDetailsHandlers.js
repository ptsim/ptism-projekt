import RequestService from 'common/services/RequestService';

export const roomDetails = roomId => RequestService.get(`/api/room/${roomId}`);

export const roomLogin = (roomId, password) =>
  RequestService.post(`/api/room/${roomId}/join`, password);

export const roomRemove = roomId => RequestService.delete(`/api/room/${roomId}`);
