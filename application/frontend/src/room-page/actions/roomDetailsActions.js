import { roomDetails, roomLogin, roomRemove } from 'room-page/handlers/roomDetailsHandlers';
import { replace } from 'react-router-redux';

export const CONNECT_TO_ROOM_PENDING = 'CONNECT_TO_ROOM_PENDING';
export const CONNECT_TO_ROOM_OK = 'CONNECT_TO_ROOM_OK';
export const CONNECT_TO_ROOM_FAIL = 'CONNECT_TO_ROOM_FAIL';

export const LOGIN_TO_ROOM_PENDING = 'LOGIN_TO_ROOM_PENDING';
export const LOGIN_TO_ROOM_OK = 'LOGIN_TO_ROOM_OK';
export const LOGIN_TO_ROOM_FAIL = 'LOGIN_TO_ROOM_FAIL';

export const REMOVE_ROOM_PENDING = 'REMOVE_ROOM_PENDING';
export const REMOVE_ROOM_OK = 'REMOVE_ROOM_OK';
export const REMOVE_ROOM_FAIL = 'REMOVE_ROOM_FAIL';

export const DEFAULT_REMOVE_ROOM_STATE = 'DEFAULT_REMOVE_ROOM_STATE';

export const makeDefaultRemoveRoomState = () => ({
  type: DEFAULT_REMOVE_ROOM_STATE,
});

export const makeConnectToRoomPending = () => ({
  type: CONNECT_TO_ROOM_PENDING,
});

export const makeConnectToRoomOk = roomData => ({
  type: CONNECT_TO_ROOM_OK,
  payload: { roomData },
});

export const makeConnectToRoomFail = () => ({
  type: CONNECT_TO_ROOM_FAIL,
});

export const makeLoginToRoomPending = () => ({
  type: LOGIN_TO_ROOM_PENDING,
});

export const makeLoginToRoomOk = () => ({
  type: LOGIN_TO_ROOM_OK,
});

export const makeLoginToRoomFail = () => ({
  type: LOGIN_TO_ROOM_FAIL,
});

export const makeRemoveRoomPending = () => ({
  type: REMOVE_ROOM_PENDING,
});

export const makeRemoveRoomOk = () => ({
  type: REMOVE_ROOM_OK,
});

export const makeRemoveRoomFail = () => ({
  type: REMOVE_ROOM_FAIL,
});

export const getRoomDetails = roomId => dispatch => {
  dispatch(makeConnectToRoomPending());

  return roomDetails(roomId)
    .then(res => {
      dispatch(makeConnectToRoomOk(res.data));
    })
    .catch(() => {
      dispatch(makeConnectToRoomFail());
    });
};

export const loginToRoom = (roomId, password) => dispatch => {
  dispatch(makeLoginToRoomPending());

  return roomLogin(roomId, password)
    .then(() => {
      dispatch(makeLoginToRoomOk());
    })
    .catch(() => {
      dispatch(makeLoginToRoomFail());
    });
};

export const removeRoom = roomId => dispatch => {
  dispatch(makeRemoveRoomPending());

  return roomRemove(roomId)
    .then(() => {
      dispatch(makeRemoveRoomOk());
    })
    .catch(() => {
      dispatch(makeRemoveRoomFail());
    });
};

export const redirectToMainPage = () => dispatch => {
  dispatch(replace('/classes'));
  dispatch(makeDefaultRemoveRoomState());
};
