import React, { Fragment } from 'react';
import { store, history } from 'store';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { injectGlobal } from 'styled-components';
import AuthService from 'AuthService';
import RootRouter from 'RootRouter';
import Layout from 'common/components/layout/Layout';
import Colors from 'common/colors';
import 'App.css';

/* eslint-disable no-unused-expressions */
injectGlobal`
  body {
    margin: 0;
    background-color: ${Colors.BACKGROUND_COLOR};
    overflow-y: hidden;
    font-family: "Roboto-Light", sans-serif !important;
  }
`;
/* eslint-enable no-unused-expressions */

const App = () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Fragment>
        <AuthService>
          <Layout>
            <RootRouter />
          </Layout>
        </AuthService>
      </Fragment>
    </ConnectedRouter>
  </Provider>
);

export default App;
