import React from 'react';
import { Route, Switch } from 'react-router-dom';
import getPath from 'common/utils/path';
import makeLoadable from 'common/utils/loadable';
import NotFoundPage from 'common/components/NotFoundPage';

export const ROOT_PATH = getPath('/');
export const ROOM_PATH = getPath('/room/:roomId');
export const PROFILE_PATH = getPath('/settings');
export const MESSAGES_PATH = getPath('/messages');
export const VISIT_PATH = getPath('/visit');
export const VISIT_DETAILS_PATH = getPath('/visit/:visitId');
export const HISTORY_PATH = getPath('/history');

export const LoadableMainPage = makeLoadable(() => import('main-page/containers/MainPage'));

export const LoadableRoomPage = makeLoadable(() => import('room-page/containers/RoomPage'));

export const LoadableProfilePage = makeLoadable(() =>
  import('profile-page/containers/ProfilePage'),
);

export const LoadableMessagesPage = makeLoadable(() =>
  import('messages-page/containers/MessagesPage'),
);

export const LoadableVisitPage = makeLoadable(() => import('visit-page/containers/VisitPage'));

export const LoadableHistoryPage = makeLoadable(() =>
  import('history-page/containers/HistoryPage'),
);

export const LoadableVisitDetailsPage = makeLoadable(() =>
  import('visit-details-page/containers/VisitDetailsPage'),
);

const RootRouter = () => (
  <Switch>
    <Route exact path={ROOT_PATH} component={LoadableMainPage} />
    <Route exact path={ROOM_PATH} component={LoadableRoomPage} />
    <Route exact path={PROFILE_PATH} component={LoadableProfilePage} />
    <Route exact path={MESSAGES_PATH} component={LoadableMessagesPage} />
    <Route exact path={VISIT_PATH} component={LoadableVisitPage} />
    <Route exact path={HISTORY_PATH} component={LoadableHistoryPage} />
    <Route exact path={VISIT_DETAILS_PATH} component={LoadableVisitDetailsPage} />
    <Route component={NotFoundPage} />
  </Switch>
);

export default RootRouter;
