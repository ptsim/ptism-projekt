import {
  CREATE_VISIT_PENDING,
  CREATE_VISIT_OK,
  CREATE_VISIT_FAIL,
  FETCH_CURRENT_VISIT_PENDING,
  FETCH_CURRENT_VISIT_OK,
  FETCH_CURRENT_VISIT_FAIL,
  FETCH_APPOINTMENT_PENDING,
  FETCH_APPOINTMENT_OK,
  FETCH_APPOINTMENT_FAIL,
} from 'visit-page/actions/visitPageActions';

const INITIAL_STATE = {
  isNewVisitLoading: false,
  isNewVisitError: false,
  isCurrentVisitLoading: true,
  isCurrentVisitError: false,
  appointmentIsLoading: true,
  appointmentIsError: false,
  visitData: [],
};

export default (state, { type, payload }) => {
  const stateDefinition = typeof state === 'undefined' ? INITIAL_STATE : state;
  switch (type) {
    case CREATE_VISIT_PENDING:
      return { ...stateDefinition, isNewVisitLoading: true, isNewVisitError: false, visitData: [] };
    case CREATE_VISIT_OK:
      return {
        ...stateDefinition,
        isNewVisitLoading: false,
        isNewVisitError: false,
        visitData: payload.visitData,
      };
    case CREATE_VISIT_FAIL:
      return { ...stateDefinition, isNewVisitLoading: false, isNewVisitError: true, visitData: [] };
    case FETCH_CURRENT_VISIT_PENDING:
      return {
        ...stateDefinition,
        isCurrentVisitLoading: true,
        isCurrentVisitError: false,
        visitData: [],
      };
    case FETCH_CURRENT_VISIT_OK:
      return {
        ...stateDefinition,
        isCurrentVisitLoading: false,
        isCurrentVisitError: false,
        visitData: payload.visitData,
      };
    case FETCH_CURRENT_VISIT_FAIL:
      return {
        ...stateDefinition,
        isCurrentVisitLoading: false,
        isCurrentVisitError: true,
        visitData: [],
      };
    case FETCH_APPOINTMENT_PENDING:
      return {
        ...stateDefinition,
        appointmentIsLoading: true,
        appointmentIsError: false,
        visitData: [],
      };
    case FETCH_APPOINTMENT_OK:
      return {
        ...stateDefinition,
        appointmentIsLoading: false,
        appointmentIsError: false,
        visitData: payload.visitData,
      };
    case FETCH_APPOINTMENT_FAIL:
      return {
        ...stateDefinition,
        appointmentIsLoading: false,
        appointmentIsError: true,
        visitData: [],
      };
    default:
      return stateDefinition;
  }
};
