import { combineReducers } from 'redux';
import newVisitReducer from 'visit-page/reducers/newVisitReducer';

export default combineReducers({
  newVisit: newVisitReducer,
});
