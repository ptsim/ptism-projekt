import RequestService from 'common/services/RequestService';

export const createNewVisit = visitData => RequestService.post(`/api/v1/appointments`, visitData);

export const checkCurrentVisit = () => RequestService.get('/api/v1/appointments/current');
