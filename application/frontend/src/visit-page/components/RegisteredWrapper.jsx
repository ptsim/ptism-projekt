/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SockJsClient from 'react-stomp';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import { fetchAppointment } from 'visit-page/actions/visitPageActions';
import inlineLoaderImage from 'images/inline-loader.svg';

const RegisteredWrapperStyled = styled.div.attrs({ className: 'registered-wrapper' })`
  text-align: center;
`;

const QueueTitle = styled.div.attrs({ className: 'queue-title' })`
  font-weight: 200;
  text-align: center;
`;

const QueueNumber = styled.div.attrs({ className: 'queue-number' })`
  display: inline-block;
  padding: 20px;
  font-weight: bold;
  font-size: 30px;
  margin: 20px 0;
  color: #fff;
  background: rgba(45, 69, 100, 0.9);
  box-shadow: 0 3px 8px rgba(0, 0, 0, 0.5);
`;

const ConnectWithDoctorButton = styled(Link).attrs({ className: 'connect-with-doctor-button' })`
  width: 30%;
  display: block;
  margin: 20px auto;
  padding: 15px;
  text-align: center;
  color: #fff;
  border-radius: 4px;
  outline: none;
  cursor: pointer;
  font-weight: bold;
  background: rgba(45, 69, 100, 0.8);
  font-size: 12px;
  transition: 0.2s;

  &:hover {
    opacity: 0.8;
  }
`;

const LoaderImage = styled.img.attrs({ className: 'loader-image' })`
  width: 20%;
  margin: 0 auto;
  display: block;
`;

const DontLeave = styled.div.attrs({ className: 'dont-leave' })`
  text-align: center;
  text-transform: uppercase;
  font-weight: 300;
  font-size: 11px;
  color: #fff;
  width: 80%;
  margin: 15px auto;
  padding: 10px 0;
  background: rgba(45, 69, 100, 0.9);
`;

const RegisteredWrapper = ({
  fetchAppointmentFunc,
  currentUserId,
  visitData,
  appointmentIsLoading,
  appointmentIsError,
}) => {
  const [lineLength, setLineLength] = useState(0);
  const [roomId, setRoomId] = useState('');

  useEffect(() => {
    setTimeout(() => {
      fetchAppointmentFunc();
    }, 500);
  }, []);

  useEffect(() => {
    if (visitData.queue !== undefined && visitData.queue !== null) {
      setLineLength(visitData.queue.number + 1);
      setRoomId(visitData.queue.roomID);
    }
  }, [appointmentIsLoading]);

  const wsSourceUrl = `${window.location.protocol}//${window.location.host}/api/chat/ws`;

  const onMessageReceive = message => {
    setLineLength(message.number);
  };

  return (
    <RegisteredWrapperStyled>
      <QueueTitle>Miejsce w kolejce</QueueTitle>
      <QueueNumber>{lineLength}</QueueNumber>
      <LoaderImage src={inlineLoaderImage} />
      <DontLeave>Nie opuszczaj tej strony aby zachować miejsce w kolejce!</DontLeave>
      {lineLength === 1 && (
        <ConnectWithDoctorButton to={`/room/${roomId}`}>Przejdź do rozmowy</ConnectWithDoctorButton>
      )}
      <SockJsClient
        url={wsSourceUrl}
        topics={[`/user/${currentUserId}/patient-queue`]}
        onMessage={onMessageReceive}
        debug
      />
    </RegisteredWrapperStyled>
  );
};

RegisteredWrapper.propTypes = {
  fetchAppointmentFunc: PropTypes.func.isRequired,
  appointmentIsLoading: PropTypes.bool.isRequired,
  appointmentIsError: PropTypes.bool.isRequired,
  currentUserId: PropTypes.string.isRequired,
  visitData: PropTypes.instanceOf(Object).isRequired,
};

const mapStateToProps = state => ({
  appointmentIsLoading: state.visit.newVisit.appointmentIsLoading,
  appointmentIsError: state.visit.newVisit.appointmentIsError,
  visitData: state.visit.newVisit.visitData,
  currentUserId: state.common.authUser.keycloakInfo.subject,
});

const mapDispatchToProps = dispatch => ({
  fetchAppointmentFunc: () => dispatch(fetchAppointment()),
});

export default connect(mapStateToProps, mapDispatchToProps)(RegisteredWrapper);
