import { createNewVisit, checkCurrentVisit } from 'visit-page/handlers/visitPageHandler';

export const CREATE_VISIT_PENDING = 'CREATE_VISIT_PENDING';
export const CREATE_VISIT_OK = 'CREATE_VISIT_OK';
export const CREATE_VISIT_FAIL = 'CREATE_VISIT_FAIL';

export const FETCH_CURRENT_VISIT_PENDING = 'FETCH_CURRENT_VISIT_PENDING';
export const FETCH_CURRENT_VISIT_OK = 'FETCH_CURRENT_VISIT_OK';
export const FETCH_CURRENT_VISIT_FAIL = 'FETCH_CURRENT_VISIT_FAIL';

export const FETCH_APPOINTMENT_PENDING = 'FETCH_APPOINTMENT_PENDING';
export const FETCH_APPOINTMENT_OK = 'FETCH_APPOINTMENT_OK';
export const FETCH_APPOINTMENT_FAIL = 'FETCH_APPOINTMENT_FAIL';

export const makeCreateVisitPending = () => ({
  type: CREATE_VISIT_PENDING,
});

export const makeCreateVisitOk = visitData => ({
  type: CREATE_VISIT_OK,
  payload: { visitData },
});

export const makeCreateVisitFail = () => ({
  type: CREATE_VISIT_FAIL,
});

export const makeFetchCurrentVisitPending = () => ({
  type: FETCH_CURRENT_VISIT_PENDING,
});

export const makeFetchCurrentVisitOk = visitData => ({
  type: FETCH_CURRENT_VISIT_OK,
  payload: { visitData },
});

export const makeFetchCurrentVisitFail = () => ({
  type: FETCH_CURRENT_VISIT_FAIL,
});

export const makeFetchAppointmentPending = () => ({
  type: FETCH_APPOINTMENT_PENDING,
});

export const makeFetchAppointmentOk = visitData => ({
  type: FETCH_APPOINTMENT_OK,
  payload: { visitData },
});

export const makeFetchAppointmentFail = () => ({
  type: FETCH_APPOINTMENT_FAIL,
});

export const newVisitCreation = visitData => dispatch => {
  dispatch(makeCreateVisitPending());

  return createNewVisit(visitData)
    .then(res => {
      dispatch(makeCreateVisitOk(res.data));
    })
    .catch(() => {
      dispatch(makeCreateVisitFail());
    });
};

export const fetchCurrentVisit = () => dispatch => {
  dispatch(makeFetchCurrentVisitPending());

  return checkCurrentVisit()
    .then(res => {
      dispatch(makeFetchCurrentVisitOk(res.data));
    })
    .catch(() => {
      dispatch(makeFetchCurrentVisitFail());
    });
};

export const fetchAppointment = () => dispatch => {
  dispatch(makeFetchAppointmentPending());

  return checkCurrentVisit()
    .then(res => {
      dispatch(makeFetchAppointmentOk(res.data));
    })
    .catch(() => {
      dispatch(makeFetchAppointmentFail());
    });
};
