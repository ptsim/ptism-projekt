/* eslint-disable no-unused-vars */
/* eslint-disable react/jsx-closing-tag-location */
import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import ProgressIndicatorCircular from 'common/components/ProgressIndicatorCircular';
import { fetchCurrentVisit, newVisitCreation } from 'visit-page/actions/visitPageActions';
import RegisteredWrapper from 'visit-page/components/RegisteredWrapper';

const VisitPageWrapper = styled.div.attrs({ className: 'visit-page-wrapper' })`
  padding: 30px;
  height: calc(100% - 60px);
  width: calc(100% - 60px);
`;

const PageName = styled.h1.attrs({ className: 'page-name' })`
  font-weight: 100;
  font-family: 'Roboto-Thin';
  margin: 0 0 10px 0;
`;

const RegisterWrapper = styled.div.attrs({ className: 'register-wrapper' })`
  position: relative;
`;

const RegisterTitle = styled.div.attrs({ className: 'register-title' })`
  text-align: center;
  text-transform: uppercase;
  font-weight: 200;
  font-size: 16px;
  margin-bottom: 20px;
`;

const VisitRegisterWrapper = styled.div.attrs({ className: 'visit-register-wrapper' })`
  background: #fff;
  border: 1px solid #ddd;
  border-radius: 4px;
  padding: 20px;
  width: calc(100% - 40px);
  margin: 0 auto;
  line-height: 25px;
  box-shadow: 0 3px 8px rgba(0, 0, 0, 0.1);
  position: relative;
  min-height: 100px;
`;

const InputLabel = styled.div.attrs({ className: 'input-label' })`
  font-size: 12px;
`;

const ConnectWithDoctorButton = styled(Link).attrs({ className: 'connect-with-doctor-button' })`
  width: 30%;
  display: block;
  margin: 20px auto;
  padding: 15px;
  text-align: center;
  color: #fff;
  border-radius: 4px;
  outline: none;
  cursor: pointer;
  font-weight: bold;
  background: rgba(45, 69, 100, 0.8);
  font-size: 12px;
  transition: 0.2s;

  &:hover {
    opacity: 0.8;
  }
`;

const StyledTextArea = styled.textarea.attrs({ className: 'styled-text-area' })`
  width: calc(100% - 40px);
  min-height: 100px;
  border-radius: 4px;
  border: 1px solid #ccc;
  padding: 20px;
  resize: none;
  outline: none;
`;

const RegisterButton = styled.button.attrs({ className: 'register-button' })`
  width: 100%;
  font-weight: 200;
  padding: 15px 10px;
  background: rgba(45, 69, 100, 0.9);
  color: #fff;
  border-radius: 4px;
  text-align: center;
  outline: none;
  border: none;
  font-size: 12px;
  transition: 0.2s;
  cursor: pointer;

  &:hover {
    opacity: 0.8;
  }

  &:disabled {
    opacity: 0.8;
    cursor: not-allowed;
  }
`;

const ErrorBlock = styled.div.attrs({ className: 'error-block' })`
  padding: 10px;
  text-align: center;
  background: #fce7e6;
  font-size: 12px;
  color: #552526;
`;

const VisitPage = ({
  fetchCurrentVisitFunc,
  newVisitCreationFunc,
  isNewVisitLoading,
  isNewVisitError,
  isCurrentVisitLoading,
  isCurrentVisitError,
  visitData,
  currentUserId,
}) => {
  const [description, setDescription] = useState('');
  const [lineLength, setLineLength] = useState(0);
  const [roomId, setRoomId] = useState('');

  useEffect(() => {
    fetchCurrentVisitFunc();
  }, []);

  useEffect(() => {
    if (visitData.queue !== undefined && visitData.queue !== null) {
      setLineLength(visitData.queue.number + 1);
      setRoomId(visitData.queue.roomID);
    }
  }, [isCurrentVisitLoading]);

  const onSend = () => {
    const visitRequestData = {
      description,
    };

    newVisitCreationFunc(visitRequestData);
  };

  if (isNewVisitError)
    return (
      <VisitPageWrapper>
        <PageName>Wiztya</PageName>
        <VisitRegisterWrapper>
          <ErrorBlock>
            Obecnie zaden doktor nie przyjmuje pacjentow, sproboj ponownie pozniej.
          </ErrorBlock>
        </VisitRegisterWrapper>
      </VisitPageWrapper>
    );

  return (
    <VisitPageWrapper>
      <PageName>Wizyta</PageName>
      <VisitRegisterWrapper>
        {isCurrentVisitLoading ? (
          <ProgressIndicatorCircular />
        ) : (
          <React.Fragment>
            {visitData === '' || visitData === [] || visitData.appointment === null ? (
              <RegisterWrapper style={isNewVisitLoading ? { opacity: '0.7' } : {}}>
                <RegisterTitle>Rejestracja</RegisterTitle>
                <InputLabel>Opisz swoją przypadłość poniżej:</InputLabel>
                <StyledTextArea
                  value={description}
                  onChange={evt => setDescription(evt.target.value)}
                />
                <RegisterButton
                  onClick={onSend}
                  disabled={description.length === 0 || isNewVisitLoading}
                >
                  {isNewVisitLoading && <ProgressIndicatorCircular />}
                  Zarejestruj się
                </RegisterButton>
              </RegisterWrapper>
            ) : (
              <RegisteredWrapper />
            )}
          </React.Fragment>
        )}
      </VisitRegisterWrapper>
    </VisitPageWrapper>
  );
};

VisitPage.propTypes = {
  fetchCurrentVisitFunc: PropTypes.func.isRequired,
  newVisitCreationFunc: PropTypes.func.isRequired,
  isNewVisitLoading: PropTypes.bool.isRequired,
  isNewVisitError: PropTypes.bool.isRequired,
  isCurrentVisitLoading: PropTypes.bool.isRequired,
  isCurrentVisitError: PropTypes.bool.isRequired,
  currentUserId: PropTypes.string.isRequired,
  visitData: PropTypes.instanceOf(Object).isRequired,
};

const mapStateToProps = state => ({
  isNewVisitLoading: state.visit.newVisit.isNewVisitLoading,
  isNewVisitError: state.visit.newVisit.isNewVisitError,
  isCurrentVisitLoading: state.visit.newVisit.isCurrentVisitLoading,
  isCurrentVisitError: state.visit.newVisit.isCurrentVisitError,
  visitData: state.visit.newVisit.visitData,
  currentUserId: state.common.authUser.keycloakInfo.subject,
});

const mapDispatchToProps = dispatch => ({
  fetchCurrentVisitFunc: () => dispatch(fetchCurrentVisit()),
  newVisitCreationFunc: visitData => dispatch(newVisitCreation(visitData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(VisitPage);
