import { combineReducers } from 'redux';
import visitDetailsReducer from 'visit-details-page/reducers/visitDetailsReducer';

export default combineReducers({
  visitDetails: visitDetailsReducer,
});
