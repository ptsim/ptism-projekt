import {
  FETCH_VISIT_BY_ID_PENDING,
  FETCH_VISIT_BY_ID_OK,
  FETCH_VISIT_BY_ID_FAIL,
  SEND_RECOMMENDATIONS_PENDING,
  SEND_RECOMMENDATIONS_OK,
  SEND_RECOMMENDATIONS_FAIL,
} from 'visit-details-page/actions/visitDetailsActions';

const INITIAL_STATE = {
  isLoading: true,
  isError: false,
  recommendationsLoading: false,
  recommendationsError: false,
  visitData: [],
  recommendationData: '',
};

export default (state, { type, payload }) => {
  const stateDefinition = typeof state === 'undefined' ? INITIAL_STATE : state;
  switch (type) {
    case FETCH_VISIT_BY_ID_PENDING:
      return { ...stateDefinition, isLoading: true, isError: false, visitData: [] };
    case FETCH_VISIT_BY_ID_OK:
      return {
        ...stateDefinition,
        isLoading: false,
        isError: false,
        visitData: payload.visitData,
      };
    case FETCH_VISIT_BY_ID_FAIL:
      return { ...stateDefinition, isLoading: false, isError: true, visitData: [] };
    case SEND_RECOMMENDATIONS_PENDING:
      return {
        ...stateDefinition,
        recommendationsLoading: true,
        recommendationsError: false,
        recommendationData: [],
      };
    case SEND_RECOMMENDATIONS_OK:
      return {
        ...stateDefinition,
        recommendationsLoading: false,
        recommendationsError: false,
        recommendationData: payload.recommendationData,
      };
    case SEND_RECOMMENDATIONS_FAIL:
      return {
        ...stateDefinition,
        recommendationsLoading: false,
        recommendationsError: true,
        recommendationData: [],
      };
    default:
      return stateDefinition;
  }
};
