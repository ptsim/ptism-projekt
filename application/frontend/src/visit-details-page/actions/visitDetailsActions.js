import {
  fetchVisitDetails,
  sendRecommendations,
} from 'visit-details-page/handlers/visitDetailsHandlers';

export const FETCH_VISIT_BY_ID_PENDING = 'FETCH_VISIT_BY_ID_PENDING';
export const FETCH_VISIT_BY_ID_OK = 'FETCH_VISIT_BY_ID_OK';
export const FETCH_VISIT_BY_ID_FAIL = 'FETCH_VISIT_BY_ID_FAIL';

export const SEND_RECOMMENDATIONS_PENDING = 'SEND_RECOMMENDATIONS_PENDING';
export const SEND_RECOMMENDATIONS_OK = 'SEND_RECOMMENDATIONS_OK';
export const SEND_RECOMMENDATIONS_FAIL = 'SEND_RECOMMENDATIONS_FAIL';

export const makeFetchVisitByIdPending = () => ({
  type: FETCH_VISIT_BY_ID_PENDING,
});

export const makeFetchVisitByIdOk = visitData => ({
  type: FETCH_VISIT_BY_ID_OK,
  payload: { visitData },
});

export const makeFetchVisitByIdFail = () => ({
  type: FETCH_VISIT_BY_ID_FAIL,
});

export const makeSendRecommendationPending = () => ({
  type: SEND_RECOMMENDATIONS_PENDING,
});

export const makeSendRecommendationOk = recommendationData => ({
  type: SEND_RECOMMENDATIONS_OK,
  payload: { recommendationData },
});

export const makeSendRecommendationFail = () => ({
  type: SEND_RECOMMENDATIONS_FAIL,
});

export const fetchVisitById = visitId => dispatch => {
  dispatch(makeFetchVisitByIdPending());

  return fetchVisitDetails(visitId)
    .then(res => {
      dispatch(makeFetchVisitByIdOk(res.data));
    })
    .catch(() => {
      dispatch(makeFetchVisitByIdFail());
    });
};

export const saveRecommendations = (visitId, data) => dispatch => {
  dispatch(makeSendRecommendationPending());

  return sendRecommendations(visitId, data)
    .then(res => {
      dispatch(makeSendRecommendationOk(res.data));
    })
    .catch(() => {
      dispatch(makeSendRecommendationFail());
    });
};
