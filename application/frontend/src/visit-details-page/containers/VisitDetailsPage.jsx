/* eslint-disable prefer-destructuring */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import ProgressIndicatorCircular from 'common/components/ProgressIndicatorCircular';
import { fetchVisitById } from 'visit-details-page/actions/visitDetailsActions';
import AddRecomendations from 'visit-details-page/components/AddRecomendations';
import Recomendations from 'visit-details-page/components/Recomendations';

const VisitDetailsPageWrapper = styled.div.attrs({ className: 'visit-details-page-wrapper' })`
  padding: 30px;
  height: calc(100% - 60px);
  width: calc(100% - 60px);
`;

const PageName = styled.h1.attrs({ className: 'page-name' })`
  font-weight: 100;
  font-family: 'Roboto-Thin';
  margin: 0 0 10px 0;
`;

const TwoSideGrid = styled.h1.attrs({ className: 'two-side-grid' })`
  display: grid;
  grid-template-columns: 40% 1fr;
`;

const VisitDetailsWrapper = styled.div.attrs({ className: 'visit-details-wrapper' })`
  background: #fff;
  border: 1px solid #ddd;
  border-radius: 4px;
  padding: 20px;
  width: calc(100% - 40px);
  margin: 0 auto;
  line-height: 25px;
  box-shadow: 0 3px 8px rgba(0, 0, 0, 0.1);
  min-height: 60px;
  position: relative;
`;

const DetailsItem = styled.div.attrs({ className: 'details-item' })`
  margin-bottom: 15px;
`;

const DetailsItemName = styled.div.attrs({ className: 'details-item-name' })`
  text-transform: uppercase;
  font-size: 10px;
  margin-bottom: 0;
  font-weight: 400;
`;

const DetailsItemContent = styled.div.attrs({ className: 'details-item-content' })`
  font-size: 15px;
`;

const VisitDetailsPage = ({ match, fetchVisitByIdFunc, isLoading, isError, visitData }) => {
  const [visitId, setVisitId] = useState();

  useEffect(() => {
    const id = match.params.visitId;
    setVisitId(id);
    fetchVisitByIdFunc(id);
  }, []);

  const monthNames = [
    'Styczeń',
    'Luty',
    'Marzec',
    'Kwiecień',
    'Maj',
    'Czerwiec',
    'Lipiec',
    'Sierpień',
    'Wrzesień',
    'Październik',
    'Listopad',
    'Grudzień',
  ];

  const dayNames = ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'];

  const addZeroBefore = n => (n < 10 ? '0' : '') + n;

  const getDate = date =>
    `${addZeroBefore(new Date(date).getDate())} ${
      monthNames[new Date(date).getMonth()]
    } ${addZeroBefore(new Date(date).getFullYear())}, ${
      dayNames[new Date(date).getDay()]
    }, ${new Date(date).getHours()}:${new Date(date).getMinutes()}`;

  if (isError)
    return (
      <VisitDetailsPageWrapper>
        <PageName>{`Wizyta - ID ${visitId}`}</PageName>
        <VisitDetailsWrapper>Wizyta o podanym ID nie istnieje.</VisitDetailsWrapper>
      </VisitDetailsPageWrapper>
    );

  return (
    <VisitDetailsPageWrapper>
      <PageName>{`Wizyta - ID ${visitId}`}</PageName>
      <VisitDetailsWrapper>
        {isLoading ? (
          <ProgressIndicatorCircular />
        ) : (
          <div>
            <TwoSideGrid>
              <div>
                <DetailsItem>
                  <DetailsItemName>Identyfikatory wizyty</DetailsItemName>
                  <DetailsItemContent>{visitData.id}</DetailsItemContent>
                </DetailsItem>
                <DetailsItem>
                  <DetailsItemName>Data wizyty</DetailsItemName>
                  <DetailsItemContent>{getDate(visitData.createdAt)}</DetailsItemContent>
                </DetailsItem>
                <DetailsItem>
                  <DetailsItemName>Opis objawów</DetailsItemName>
                  <DetailsItemContent>{visitData.description}</DetailsItemContent>
                </DetailsItem>
              </div>
              <div>
                <DetailsItem>
                  <DetailsItemName>Rekomendacje</DetailsItemName>
                  <DetailsItemContent>
                    {visitData.recommendations === null ? (
                      <AddRecomendations visitId={visitId} />
                    ) : (
                      <Recomendations data={visitData.recommendations} />
                    )}
                  </DetailsItemContent>
                </DetailsItem>
              </div>
            </TwoSideGrid>
          </div>
        )}
      </VisitDetailsWrapper>
    </VisitDetailsPageWrapper>
  );
};

VisitDetailsPage.propTypes = {
  fetchVisitByIdFunc: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  visitData: PropTypes.instanceOf(Object).isRequired,
  match: PropTypes.instanceOf(Object).isRequired,
};

const mapStateToProps = state => ({
  isLoading: state.visitDetails.visitDetails.isLoading,
  isError: state.visitDetails.visitDetails.isError,
  visitData: state.visitDetails.visitDetails.visitData,
});

const mapDispatchToProps = dispatch => ({
  fetchVisitByIdFunc: visitId => dispatch(fetchVisitById(visitId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(VisitDetailsPage);
