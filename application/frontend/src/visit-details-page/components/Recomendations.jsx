import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const RecomendationsWrapper = styled.textarea.attrs({ className: 'recommendations-wrapper' })`
  border: 1px solid #ccc;
  border-radius: 4px;
  padding: 10px;
  font-size: 12px;
  resize: none;
  width: calc(100% - 20px);
`;

const Recomendations = ({ data }) => <RecomendationsWrapper value={data} disabled />;

Recomendations.propTypes = {
  data: PropTypes.string.isRequired,
};

export default Recomendations;
