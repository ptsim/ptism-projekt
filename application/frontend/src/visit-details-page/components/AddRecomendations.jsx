import React, { useState } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { saveRecommendations } from 'visit-details-page/actions/visitDetailsActions';
import RealmRolesService from 'common/authorization/RealmRolesService';
import realmRoles from 'common/authorization/realmRoles';
import ButtonProgressIndicator from 'common/components/ButtonProgressIndicator';

const AddRecomendationsWrapper = styled.div.attrs({
  className: 'add-recommendations-wrapper',
})``;

const NoDoctor = styled.div.attrs({
  className: 'no-doctor',
})``;

const SendButton = styled.button.attrs({
  className: 'no-doctor',
})`
  font-weight: 200;
  padding: 15px 10px;
  background: rgba(45, 69, 100, 0.9);
  color: #fff;
  border-radius: 4px;
  text-align: center;
  outline: none;
  border: none;
  font-size: 12px;
  transition: 0.2s;
  cursor: pointer;
  display: block;
  width: 100%;

  &:hover {
    opacity: 0.8;
  }

  &:disabled {
    opacity: 0.8;
    cursor: not-allowed;
  }
`;

const AddRecommendationsTextArea = styled.textarea.attrs({
  className: 'add-recommendations-wrapper',
})`
  border: 1px solid #ccc;
  border-radius: 4px;
  padding: 10px;
  font-size: 12px;
  resize: none;
  outline: none;
  width: calc(100% - 20px);
`;

const AddRecomendations = ({
  saveRecommendationsFunc,
  recommendationData,
  recommendationsLoading,
  visitId,
}) => {
  const [recommendations, setRecommendations] = useState('');

  const isDoctor = () => RealmRolesService.hasRole(realmRoles.ROLE_DOCTOR);

  const onClick = () => {
    const requestData = {
      recommendations,
    };

    saveRecommendationsFunc(visitId, requestData);
  };

  return (
    <AddRecomendationsWrapper>
      {isDoctor ? (
        <React.Fragment>
          <AddRecommendationsTextArea
            disabled={recommendationData !== ''}
            value={recommendations}
            onChange={event => setRecommendations(event.target.value)}
          />
          <SendButton
            disabled={
              recommendations.length === 0 || recommendationsLoading || recommendationData !== ''
            }
            onClick={onClick}
          >
            {recommendationsLoading ? <ButtonProgressIndicator /> : 'Zapisz'}
          </SendButton>
        </React.Fragment>
      ) : (
        <NoDoctor>Nie dodano jeszcze rekomendacji dla pacjenta.</NoDoctor>
      )}
    </AddRecomendationsWrapper>
  );
};

AddRecomendations.propTypes = {
  recommendationData: PropTypes.func.isRequired,
  saveRecommendationsFunc: PropTypes.func.isRequired,
  recommendationsLoading: PropTypes.bool.isRequired,
  visitId: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  recommendationsLoading: state.visitDetails.visitDetails.recommendationsLoading,
  recommendationsError: state.visitDetails.visitDetails.recommendationsError,
  recommendationData: state.visitDetails.visitDetails.recommendationData,
});

const mapDispatchToProps = dispatch => ({
  saveRecommendationsFunc: (visitId, data) => dispatch(saveRecommendations(visitId, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddRecomendations);
