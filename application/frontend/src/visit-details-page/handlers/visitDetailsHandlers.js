import RequestService from 'common/services/RequestService';

export const fetchVisitDetails = appointmentId =>
  RequestService.get(`/api/v1/appointments/${appointmentId}`);

export const sendRecommendations = (appointmentId, data) =>
  RequestService.put(`/api/v1/appointments/${appointmentId}`, data);
