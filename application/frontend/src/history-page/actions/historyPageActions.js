import { fetchVisitHistory } from 'history-page/handlers/historyPageHandlers';

export const FETCH_VISITS_HISTORY_PENDING = 'FETCH_VISITS_HISTORY_PENDING';
export const FETCH_VISITS_HISTORY_OK = 'FETCH_VISITS_HISTORY_OK';
export const FETCH_VISITS_HISTORY_FAIL = 'FETCH_VISITS_HISTORY_FAIL';

export const makeFetchVisitsHistoryPending = () => ({
  type: FETCH_VISITS_HISTORY_PENDING,
});

export const makeFetchVisitsHistoryOk = visitsList => ({
  type: FETCH_VISITS_HISTORY_OK,
  payload: { visitsList },
});

export const makeFetchVisitsHistoryFail = () => ({
  type: FETCH_VISITS_HISTORY_FAIL,
});

export const fetchVisitsList = () => dispatch => {
  dispatch(makeFetchVisitsHistoryPending());

  return fetchVisitHistory()
    .then(res => {
      dispatch(makeFetchVisitsHistoryOk(res.data));
    })
    .catch(() => {
      dispatch(makeFetchVisitsHistoryFail());
    });
};
