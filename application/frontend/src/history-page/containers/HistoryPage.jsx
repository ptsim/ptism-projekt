/* eslint-disable no-unused-vars */
import React, { useEffect } from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import ProgressIndicatorCircular from 'common/components/ProgressIndicatorCircular';
import { fetchVisitsList } from 'history-page/actions/historyPageActions';

const HistoryPageWrapper = styled.div.attrs({ className: 'history-page-wrapper' })`
  padding: 30px;
  height: calc(100% - 60px);
  width: calc(100% - 60px);
`;

const PageName = styled.h1.attrs({ className: 'page-name' })`
  font-weight: 100;
  font-family: 'Roboto-Thin';
  margin: 0 0 10px 0;
`;

const NoEntries = styled.div.attrs({ className: 'no-entries' })`
  text-align: center;
  font-weight: 200;
  font-size: 14px;
  margin: 30px 0;
`;

const VisitsListWrapper = styled.div.attrs({ className: 'visits-list-wrapper' })`
  background: #fff;
  border: 1px solid #ddd;
  border-radius: 4px;
  padding: 20px;
  width: calc(100% - 40px);
  margin: 0 auto;
  line-height: 25px;
  box-shadow: 0 3px 8px rgba(0, 0, 0, 0.1);
  min-height: 60px;
  position: relative;
`;

const VisitsList = styled.ul.attrs({ className: 'visits-list' })`
  margin: 0;
  padding: 0;
  list-style-type: none;
`;

const VisitsListItem = styled.li.attrs({ className: 'visits-list-item' })`
  display: grid;
  grid-template-columns: 25% repeat(3, 1fr);
  font-size: 12px;
  padding: 10px;
  width: calc(100% - 20px);
  border-bottom: 1px solid #ccc;
  transition: 0.2s;
  font-weight: 300;
`;

const VisitsListItemLink = styled(Link).attrs({ className: 'visits-list-item-link' })`
  display: grid;
  grid-template-columns: 25% repeat(3, 1fr);
  font-size: 12px;
  padding: 10px;
  width: calc(100% - 20px);
  border-bottom: 1px solid #ccc;
  transition: 0.2s;
  cursor: pointer;
  font-weight: 300;
  color: #000;
  text-decoration: none;

  &:nth-child(2n + 1) {
    background: #f9f9f9;
  }

  &:first-child {
    &:hover {
      background: #fff;
      cursor: default;
    }
  }

  &:hover {
    background: #eee;
  }

  &:last-child {
    border: none;
  }
`;

const DoctorDetails = styled.div.attrs({ className: 'doctor-details' })`
  display: flex;
`;

const DoctorName = styled.div.attrs({ className: 'doctor-name' })`
  font-size: 12px;
`;

const ListHeader = styled.div.attrs({ className: 'list-header' })`
  font-weight: 600;
`;

const UserCircle = styled.div.attrs({ className: 'user-circle' })`
  display: block;
  border-radius: 50%;
  width: 25px;
  height: 25px;
  line-height: 25px;
  margin-right: 10px;
  color: #ffffff;
  background: #2d4564;
  text-align: center;
  font-size: 9px;
  font-weight: 400;
`;

const monthNames = [
  'Styczeń',
  'Luty',
  'Marzec',
  'Kwiecień',
  'Maj',
  'Czerwiec',
  'Lipiec',
  'Sierpień',
  'Wrzesień',
  'Październik',
  'Listopad',
  'Grudzień',
];

const dayNames = ['Niedziela', 'Poniedziałek', 'Wtorek', 'Środa', 'Czwartek', 'Piątek', 'Sobota'];

const addZeroBefore = n => (n < 10 ? '0' : '') + n;

const getDate = date =>
  `${addZeroBefore(new Date(date).getDate())} ${
    monthNames[new Date(date).getMonth()]
  } ${addZeroBefore(new Date(date).getFullYear())}, ${
    dayNames[new Date(date).getDay()]
  }, ${new Date(date).getHours()}:${new Date(date).getMinutes()}`;

const HEADERS = ['ID wizyty', 'Doktor', 'Data wizyty'];

const HistoryPage = ({ fetchVisitsListFunc, isError, isLoading, visitsList }) => {
  useEffect(() => {
    fetchVisitsListFunc();
  }, []);

  const getCircleData = doctor => doctor.charAt(0);

  return (
    <HistoryPageWrapper>
      <PageName>Historia przyjęć</PageName>
      <VisitsListWrapper>
        {isLoading ? (
          <ProgressIndicatorCircular />
        ) : (
          <VisitsList>
            <VisitsListItem>
              {HEADERS.map(item => (
                <ListHeader key={item}>{item}</ListHeader>
              ))}
            </VisitsListItem>
            {visitsList.length > 0 ? (
              <React.Fragment>
                {visitsList.map(visitItem => (
                  <VisitsListItemLink to={`/visit/${visitItem.id}`} key={visitItem.id}>
                    <div>{visitItem.id}</div>
                    <DoctorDetails>
                      <UserCircle>{getCircleData('D')}</UserCircle>
                      <DoctorName> Doktor Przemek</DoctorName>
                    </DoctorDetails>
                    <div>{getDate(visitItem.createdAt)}</div>
                  </VisitsListItemLink>
                ))}
              </React.Fragment>
            ) : (
              <NoEntries>Nie masz jeszcze zadnych wizyt w histori.</NoEntries>
            )}
          </VisitsList>
        )}
      </VisitsListWrapper>
    </HistoryPageWrapper>
  );
};

HistoryPage.propTypes = {
  fetchVisitsListFunc: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  visitsList: PropTypes.instanceOf(Object).isRequired,
};

const mapStateToProps = state => ({
  isLoading: state.visitsHistory.visitHistory.isLoading,
  isError: state.visitsHistory.visitHistory.isError,
  visitsList: state.visitsHistory.visitHistory.visitsList,
});

const mapDispatchToProps = dispatch => ({
  fetchVisitsListFunc: () => dispatch(fetchVisitsList()),
});

export default connect(mapStateToProps, mapDispatchToProps)(HistoryPage);
