/* eslint-disable import/prefer-default-export */
import RequestService from 'common/services/RequestService';

export const fetchVisitHistory = () => RequestService.get(`/api/v1/appointments`);
