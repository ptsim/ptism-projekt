import { combineReducers } from 'redux';
import historyReducer from 'history-page/reducers/historyReducer';

export default combineReducers({
  visitHistory: historyReducer,
});
