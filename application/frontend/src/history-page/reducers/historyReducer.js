import {
  FETCH_VISITS_HISTORY_PENDING,
  FETCH_VISITS_HISTORY_OK,
  FETCH_VISITS_HISTORY_FAIL,
} from 'history-page/actions/historyPageActions';

const INITIAL_STATE = {
  isLoading: true,
  isError: false,
  visitsList: [],
};

export default (state, { type, payload }) => {
  const stateDefinition = typeof state === 'undefined' ? INITIAL_STATE : state;
  switch (type) {
    case FETCH_VISITS_HISTORY_PENDING:
      return { ...stateDefinition, isLoading: true, isError: false, visitsList: [] };
    case FETCH_VISITS_HISTORY_OK:
      return {
        ...stateDefinition,
        isLoading: false,
        isError: false,
        visitsList: payload.visitsList,
      };
    case FETCH_VISITS_HISTORY_FAIL:
      return { ...stateDefinition, isLoading: false, isError: true, visitsList: [] };
    default:
      return stateDefinition;
  }
};
