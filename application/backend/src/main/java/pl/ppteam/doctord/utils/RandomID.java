package pl.ppteam.doctord.utils;

import java.util.UUID;

public class RandomID {
    public static String randomID() {
        return UUID.randomUUID().toString();
    }
}
