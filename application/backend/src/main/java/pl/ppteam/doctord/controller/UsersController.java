package pl.ppteam.doctord.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.ppteam.doctord.controller.dto.UserDto;
import pl.ppteam.doctord.service.UserService;
import pl.ppteam.doctord.utils.Logger;
import reactor.core.publisher.Flux;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/users")
public class UsersController {
    private static final Logger LOGGER = new Logger();

    private final UserService userService;

    /**
     * Listing all users within application and returns the ones matching search query.
     *
     * @param search      query to be searched
     * @param firstResult number of results to begin the return from
     * @param maxResults  maximum size of results to return
     * @return Flux<UserDto> - list of all users taken from Keycloak
     */
    @GetMapping("/")
    public Flux<UserDto> getAll(
            @RequestParam(value = "search", required = false) String search,
            @RequestParam(value = "first", defaultValue = "0") Integer firstResult,
            @RequestParam(value = "max", defaultValue = "15") Integer maxResults
    ) {
        LOGGER.info("Finding all users that match query '{}'", search);
        return userService.getAll(search, firstResult, maxResults);
    }
}


