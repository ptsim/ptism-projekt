package pl.ppteam.doctord.security;

public final class KeycloakRoles {
    public static final String ROLE_PATIENT = "ROLE_app-patient";
    public static final String ROLE_DOCTOR = "ROLE_app-doctor";
    public static final String ROLE_ADMIN = "ROLE_admin";

    private KeycloakRoles() {
        throw new IllegalStateException("Utility class");
    }
}

