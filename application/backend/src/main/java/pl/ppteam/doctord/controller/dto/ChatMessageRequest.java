package pl.ppteam.doctord.controller.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mongodb.lang.Nullable;
import lombok.Data;
import pl.ppteam.doctord.entity.chat.ChatType;

import java.time.LocalDateTime;

@Data
public class ChatMessageRequest {
    @Nullable
    @JsonProperty("type")
    private ChatType type;

    @Nullable
    @JsonProperty("content")
    private String content;

    @Nullable
    @JsonProperty("sender")
    private String sender;

    @Nullable
    @JsonProperty("receiver")
    private String receiver;

    @Nullable
    @JsonProperty("read")
    private boolean read;

    @Nullable
    @JsonProperty("dateTime")
    private LocalDateTime dateTime;
}
