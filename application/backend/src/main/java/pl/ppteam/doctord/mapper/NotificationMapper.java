package pl.ppteam.doctord.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.ppteam.doctord.controller.dto.NotificationItem;
import pl.ppteam.doctord.controller.dto.NotificationRequest;
import pl.ppteam.doctord.entity.notification.Notification;

@Mapper
public interface NotificationMapper {
    NotificationMapper INSTANCE = Mappers.getMapper(NotificationMapper.class);

    NotificationItem createNotificationToNotificationItem(Notification notification);

    Notification createNotificationRequestToNotification(NotificationRequest notificationRequest);
}
