package pl.ppteam.doctord.entity;

public final class ValidationConstants {

    public final static int MAX_ROOM_NAME_LENGTH = 32;
    public final static int MIN_ROOM_NAME_LENGTH = 4;

    private ValidationConstants() {
        throw new IllegalStateException("Utility class");
    }
}
