package pl.ppteam.doctord.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.ppteam.doctord.controller.dto.NotificationItem;
import pl.ppteam.doctord.service.NotificationsService;
import pl.ppteam.doctord.utils.Logger;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static pl.ppteam.doctord.security.SecurityUtils.getCurrentUsername;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/notifications")
public class NotificationsController {
    private static final Logger LOGGER = new Logger();

    private final NotificationsService notificationsService;

    /**
     * Getting a list of all notifications for current user.
     *
     * @return Flux<NotificationItem> - list of available notifications
     */
    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<NotificationItem> getNotificationsList() {
        LOGGER.info("Getting notifications list for user {}.", getCurrentUsername());
        return notificationsService.getNotificationsList();
    }

    /**
     * Checking if any new notifications are available for current user.
     *
     * @return true/false - boolean depending on result of query
     */
    @GetMapping(value = "/new")
    public Mono<Boolean> checkIfAnyNewNotifications() {
        LOGGER.info("Checking if any new notifications for user {}.", getCurrentUsername());
        return notificationsService.checkIfAnyNewNotifications();
    }
}
