package pl.ppteam.doctord.service.implementation;

import lombok.RequiredArgsConstructor;
import org.keycloak.admin.client.resource.UsersResource;
import org.springframework.stereotype.Service;
import pl.ppteam.doctord.controller.dto.UserDto;
import pl.ppteam.doctord.service.UserService;
import reactor.core.publisher.Flux;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UsersResource usersResource;

    @Override
    public Flux<UserDto> getAll(String search, Integer firstResult, Integer maxResults) {
        return Flux.fromIterable(usersResource.search(search, firstResult, maxResults))
                .map(usr ->
                        new UserDto(
                                usr.getFirstName(),
                                usr.getLastName(),
                                usr.getUsername()
                        )
                );
    }
}
