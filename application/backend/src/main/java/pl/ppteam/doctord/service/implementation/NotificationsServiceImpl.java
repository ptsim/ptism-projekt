package pl.ppteam.doctord.service.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import pl.ppteam.doctord.controller.dto.NotificationItem;
import pl.ppteam.doctord.controller.dto.NotificationRequest;
import pl.ppteam.doctord.security.SecurityUtils;
import pl.ppteam.doctord.entity.notification.Notification;
import pl.ppteam.doctord.entity.notification.NotificationType;
import pl.ppteam.doctord.service.NotificationsService;
import pl.ppteam.doctord.mapper.NotificationMapper;
import pl.ppteam.doctord.repository.NotificationRepository;
import pl.ppteam.doctord.utils.Logger;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class NotificationsServiceImpl implements NotificationsService {

    private static final Logger LOGGER = new Logger();

    private final NotificationRepository notificationRepository;
    private final NotificationMapper notificationMapper;
    private final SimpMessagingTemplate simpMessagingTemplate;

    @Override
    public Flux<NotificationItem> getNotificationsList() {
        return notificationRepository.findAllByIdNotNullAndAuthorNotOrderByDateTimeDesc(SecurityUtils.getCurrentUsername())
                .map(notificationMapper::createNotificationToNotificationItem);
    }

    @Override
    public Notification sendNotification(String author, NotificationType notificationType) {
        NotificationRequest notificationRequest = createNotificationRequest(author, notificationType);

        simpMessagingTemplate.convertAndSend("/notify", notificationRequest);

        LOGGER.info("Adding new notification by user {}.", author);

        return notificationRepository
                .save(notificationMapper.createNotificationRequestToNotification(notificationRequest)).block();
    }

    @Override
    public Mono<Boolean> checkIfAnyNewNotifications() {
        return notificationRepository.existsByIdNotNullAndReadFalseAndAuthorNot(SecurityUtils.getCurrentUsername());
    }

    private NotificationRequest createNotificationRequest(String author, NotificationType notificationType) {
        String notificationContent = createNotificationContent(notificationType);

        return new NotificationRequest(
                notificationType,
                author,
                notificationContent,
                false,
                LocalDateTime.now()
        );
    }

    private String createNotificationContent(NotificationType notificationType) {
        switch (notificationType) {
            case ROOM_CREATED:
                return "has just created a room";
            case ROOM_DELETED:
                return "has just deleted a room";
            default:
                return "error";
        }
    }
}
