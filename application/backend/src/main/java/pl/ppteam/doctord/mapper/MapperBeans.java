package pl.ppteam.doctord.mapper;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperBeans {

    @Bean
    public ChatMapper chatMapper() {
        return ChatMapper.INSTANCE;
    }

    @Bean
    public NotificationMapper notificationMapper() {
        return NotificationMapper.INSTANCE;
    }
}
