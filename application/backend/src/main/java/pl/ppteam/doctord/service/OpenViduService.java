package pl.ppteam.doctord.service;

import org.springframework.http.ResponseEntity;
import pl.ppteam.doctord.openvidu.controller.request.LeaveSessionRequest;
import pl.ppteam.doctord.openvidu.controller.request.SessionRequest;

public interface OpenViduService {
    ResponseEntity<?> getToken(SessionRequest sessionRequest);

    ResponseEntity<?> removeUser(LeaveSessionRequest leaveSessionRequest);
}
