package pl.ppteam.doctord.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.ppteam.doctord.controller.dto.ChatMessageItem;
import pl.ppteam.doctord.controller.dto.ChatMessageRequest;
import pl.ppteam.doctord.entity.chat.ChatMessage;
import pl.ppteam.doctord.service.ChatService;
import pl.ppteam.doctord.utils.Logger;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

import static pl.ppteam.doctord.security.SecurityUtils.getCurrentUsername;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/chat")
public class ChatController {
    private static final Logger LOGGER = new Logger();

    private final ChatService chatService;

    /**
     * Sending a private message to another user.
     *
     * @param chatMessageRequest - body of send message request
     * @return ChatMessage - message object
     */
    @MessageMapping("/sendPrivateMessage")
    public Mono<ChatMessage> sendPrivateMessage(@Payload ChatMessageRequest chatMessageRequest) {
        LOGGER.info("Sending private message with content: {}.", chatMessageRequest.getContent());
        return chatService.sendPrivateMessage(chatMessageRequest);
    }

    /**
     * Getting list of all messages for current user on messages page sidebar.
     *
     * @return List<ChatMessage> - list of message objects
     */
    @GetMapping(value = "/messages", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ChatMessageItem> getMessagesList() {
        LOGGER.info("Getting messages list for user {}.", getCurrentUsername());
        return chatService.getMessagesList();
    }

    /**
     * Fetching chat history for current user with user with given username.
     *
     * @param username - another user username
     * @return Flux<ChatMessage> - list of message objects
     */
    @GetMapping(value = "/history", produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<ChatMessage> getChatHistoryWithUser(@RequestParam(value = "username") String username) {
        LOGGER.info("Getting chat history for users {} and {}.", username, getCurrentUsername());
        return chatService.getChatHistoryWithUser(username);
    }

    /**
     * Getting a count of unread messages for current user.
     *
     * @return Mono<Integer> - count of unread messages
     */
    @GetMapping(value = "/count", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Integer> countUnreadMessages() {
        LOGGER.info("Counting unread messages by user {}.", getCurrentUsername());
        return chatService.countUnreadMessages();
    }

    /**
     * Marking all messages as already read for current user with user with given username.
     *
     * @param username - another user username
     * @return Flux<ChatMessage> - list of message objects
     */
    @GetMapping(value = "/read", produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<ChatMessage> markMessagesWithUserAsRead(@RequestParam(value = "username") String username) {
        LOGGER.info("Marking messages as read by user {} with user {}.", getCurrentUsername(), username);
        return chatService.markMessagesWithUserAsRead(username);
    }
}