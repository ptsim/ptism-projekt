package pl.ppteam.doctord.controller.error.exception;

import org.springframework.http.HttpStatus;

public class RoomNotFoundException extends DomainException {
    public RoomNotFoundException() {
        super(HttpStatus.NOT_FOUND, "Room not found");
    }
}
