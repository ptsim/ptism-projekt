package pl.ppteam.doctord.service;

import pl.ppteam.doctord.controller.dto.ChatMessageItem;
import pl.ppteam.doctord.controller.dto.ChatMessageRequest;
import pl.ppteam.doctord.entity.chat.ChatMessage;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface ChatService {
    Mono<ChatMessage> sendPrivateMessage(ChatMessageRequest chatMessageRequest);

    Flux<ChatMessage> getChatHistoryWithUser(String username);

    List<ChatMessageItem> getMessagesList();

    Mono<Integer> countUnreadMessages();

    Flux<ChatMessage> markMessagesWithUserAsRead(String username);
}
