package pl.ppteam.doctord.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.ppteam.doctord.controller.dto.ChatMessageRequest;
import pl.ppteam.doctord.entity.chat.ChatMessage;

@Mapper
public interface ChatMapper {
    ChatMapper INSTANCE = Mappers.getMapper(ChatMapper.class);

    ChatMessage createRequestToChatMessage(ChatMessageRequest request);
}
