package pl.ppteam.doctord.service.implementation;

import lombok.RequiredArgsConstructor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import pl.ppteam.doctord.controller.dto.ChatMessageItem;
import pl.ppteam.doctord.controller.dto.ChatMessageRequest;
import pl.ppteam.doctord.security.SecurityUtils;
import pl.ppteam.doctord.entity.chat.ChatMessage;
import pl.ppteam.doctord.entity.chat.ChatType;
import pl.ppteam.doctord.mapper.ChatMapper;
import pl.ppteam.doctord.repository.ChatRepository;
import pl.ppteam.doctord.service.ChatService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ChatServiceImpl implements ChatService {
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final ChatRepository chatRepository;
    private final ChatMapper chatMapper;

    @Override
    public Mono<ChatMessage> sendPrivateMessage(ChatMessageRequest chatMessageRequest) {
        ChatMessage chatMessage = chatMapper.createRequestToChatMessage(chatMessageRequest);

        simpMessagingTemplate.convertAndSendToUser(
                chatMessage.getReceiver().trim(), "/reply", chatMessage);

        if (chatMessage.getType() == ChatType.TYPING || chatMessage.getType() == ChatType.STOP_TYPING) {
            return null;
        }

        return chatRepository.save(chatMessage);
    }

    @Override
    public Flux<ChatMessage> getChatHistoryWithUser(String username) {
        return chatRepository.findAllBySenderEqualsAndReceiverEquals(SecurityUtils.getCurrentUsername(), username)
                .concatWith(chatRepository.findAllBySenderEqualsAndReceiverEquals(username, SecurityUtils.getCurrentUsername()))
                .sort(Comparator.comparing(ChatMessage::getDateTime));
    }

    @Override
    public Mono<Integer> countUnreadMessages() {
        return chatRepository.countAllByIdNotNullAndReceiverEqualsAndReadEquals(SecurityUtils.getCurrentUsername(), false);
    }

    @Override
    public Flux<ChatMessage> markMessagesWithUserAsRead(String username) {
        return chatRepository
                .findAllByIdNotNullAndReceiverEqualsAndSenderEqualsAndReadEquals(SecurityUtils.getCurrentUsername(), username, false)
                .map(chatMessage -> {
                    chatMessage.setRead(true);
                    return chatMessage;
                }).flatMap(chatRepository::save);
    }

    @Override
    public List<ChatMessageItem> getMessagesList() {
        Flux<ChatMessage> messagesList =
                chatRepository.findAllByReceiverEqualsOrSenderEqualsOrderByDateTimeDesc(
                        SecurityUtils.getCurrentUsername(), SecurityUtils.getCurrentUsername());

        List<ChatMessage> filteredList = Objects.requireNonNull(messagesList.collectList().block()).stream()
                .filter(distinctByKeys(ChatMessage::getReceiver, ChatMessage::getSender))
                .collect(Collectors.toList());

        List<ChatMessage> toDelete = new ArrayList<>();

        for (ChatMessage message : filteredList) {
            for (ChatMessage messageInner : filteredList) {
                if (message.getSender().equals(messageInner.getReceiver())
                        && message.getReceiver().equals(messageInner.getSender())) {
                    if (message.getDateTime().isBefore(messageInner.getDateTime())) {
                        toDelete.add(message);
                    } else if (messageInner.getDateTime().isBefore(message.getDateTime())) {
                        toDelete.add(messageInner);
                    }
                }
            }
        }

        filteredList.removeAll(toDelete);

        for (ChatMessage message : filteredList) {
            if (message.getSender().equals(SecurityUtils.getCurrentUsername())) {
                message.setSender(message.getReceiver());
            }
        }

        List<ChatMessageItem> returnList = new ArrayList<>();

        boolean anyMessageUnRead = chatRepository.
                existsByIdNotNullAndReadEqualsAndReceiverEquals(false, SecurityUtils.getCurrentUsername()).block();

        for (ChatMessage message : filteredList) {
            returnList.add(new ChatMessageItem(
                    message.getId(),
                    message.getSender(),
                    message.getDateTime(),
                    message.getContent(),
                    anyMessageUnRead,
                    true
            ));
        }

        return returnList;
    }

    private static <T> Predicate<T> distinctByKeys(Function<? super T, ?>... keyExtractors) {
        final Map<List<?>, Boolean> seen = new ConcurrentHashMap<>();

        return t -> {
            final List<?> keys = Arrays.stream(keyExtractors)
                    .map(ke -> ke.apply(t))
                    .collect(Collectors.toList());

            return seen.putIfAbsent(keys, Boolean.TRUE) == null;
        };
    }
}
