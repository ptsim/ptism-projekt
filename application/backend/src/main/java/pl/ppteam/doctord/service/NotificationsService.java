package pl.ppteam.doctord.service;

import pl.ppteam.doctord.controller.dto.NotificationItem;
import pl.ppteam.doctord.entity.notification.Notification;
import pl.ppteam.doctord.entity.notification.NotificationType;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface NotificationsService {
    Flux<NotificationItem> getNotificationsList();

    Notification sendNotification(String author, NotificationType notificationType);

    Mono<Boolean> checkIfAnyNewNotifications();

}
