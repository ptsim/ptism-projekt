package pl.ppteam.doctord.entity.chat;

public enum ChatType {
    CHAT,
    TYPING,
    STOP_TYPING,
    LEAVE
}
