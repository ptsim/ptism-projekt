package pl.ppteam.doctord.service;

import pl.ppteam.doctord.controller.dto.UserDto;
import reactor.core.publisher.Flux;

public interface UserService {
    Flux<UserDto> getAll(String search, Integer firstResult, Integer maxResults);
}
