package pl.ppteam.doctord.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import pl.ppteam.doctord.entity.chat.ChatMessage;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Repository
public interface ChatRepository extends ReactiveMongoRepository<ChatMessage, String> {
    Flux<ChatMessage> findAllBySenderEqualsAndReceiverEquals(String sender, String receiver);

    Flux<ChatMessage> findAllByReceiverEqualsOrSenderEqualsOrderByDateTimeDesc(String receiver, String sender);

    Mono<Boolean> existsByIdNotNullAndReadEqualsAndReceiverEquals(boolean read, String receiver);

    Mono<Integer> countAllByIdNotNullAndReceiverEqualsAndReadEquals(String receiver, boolean read);

    Flux<ChatMessage> findAllByIdNotNullAndReceiverEqualsAndSenderEqualsAndReadEquals(String receiver, String sender, boolean read);
}
