package pl.ppteam.doctord.service.implementation

import io.openvidu.java.client.OpenVidu
import io.openvidu.java.client.OpenViduHttpException
import io.openvidu.java.client.OpenViduJavaClientException
import io.openvidu.java.client.OpenViduRole
import io.openvidu.java.client.Session
import io.openvidu.java.client.SessionProperties
import io.openvidu.java.client.TokenOptions
import org.json.simple.JSONObject
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import pl.ppteam.doctord.openvidu.controller.request.LeaveSessionRequest
import pl.ppteam.doctord.openvidu.controller.request.SessionRequest
import pl.ppteam.doctord.security.SecurityUtils
import pl.ppteam.doctord.service.OpenViduService
import pl.ppteam.doctord.utils.Logger
import java.util.concurrent.ConcurrentHashMap

@Service
class OpenViduServiceImpl(
    @Value("\${openvidu.secret}") secret: String,
    @Value("\${openvidu.url}") openViduUrl: String,
) : OpenViduService {

    private val openVidu: OpenVidu = OpenVidu(openViduUrl, secret)

    private val mapSessions: MutableMap<String, Session> = ConcurrentHashMap()
    private val mapSessionNamesTokens: MutableMap<String, MutableMap<String, OpenViduRole>> = ConcurrentHashMap()

    override fun getToken(sessionRequest: SessionRequest): ResponseEntity<*> {
        val sessionName: String = sessionRequest.sessionName
        val role: OpenViduRole = sessionRequest.role
        val serverData = """{"serverData": "${sessionRequest.username}"}"""
        val tokenOptions = TokenOptions.Builder().data(serverData).role(role).build()
        val responseJson = JSONObject()
        if (mapSessions[sessionName] != null) {
            LOGGER.info("Existing session: $sessionName")
            try {
                val token = mapSessions[sessionName]!!.generateToken(tokenOptions)
                mapSessionNamesTokens[sessionName]!![token] = role
                responseJson[0.toString()] = token
                return ResponseEntity(responseJson, HttpStatus.OK)
            } catch (e1: OpenViduJavaClientException) {
                return getErrorResponse(e1)
            } catch (e2: OpenViduHttpException) {
                if (404 == e2.status) {
                    mapSessions.remove(sessionName)
                    mapSessionNamesTokens.remove(sessionName)
                }
            }
        }
        LOGGER.info("New session: $sessionName")
        return try {
            val properties = SessionProperties.Builder().build()
            val session = openVidu.createSession(properties)
            val token = session.generateToken(tokenOptions)
            mapSessions[sessionName] = session
            mapSessionNamesTokens[sessionName] = ConcurrentHashMap()
            mapSessionNamesTokens[sessionName]!![token] = role
            responseJson[0.toString()] = token
            ResponseEntity(responseJson, HttpStatus.OK)
        } catch (e: Exception) {
            e.printStackTrace()
            getErrorResponse(e)
        }
    }

    override fun removeUser(leaveSessionRequest: LeaveSessionRequest): ResponseEntity<*> {
        val sessionName: String = leaveSessionRequest.sessionName
        val token: String = leaveSessionRequest.token
        return if (mapSessions[sessionName] != null && mapSessionNamesTokens[sessionName] != null) {
            if (mapSessionNamesTokens[sessionName]!!.remove(token) != null) {
                if (mapSessionNamesTokens[sessionName]!!.isEmpty()) {
                    mapSessions.remove(sessionName)
                }
                ResponseEntity(HttpStatus.OK)
            } else {
                LOGGER.error("Trying to leave session by user {}. Given token was not valid",
                    SecurityUtils.getCurrentUsername())
                ResponseEntity<Any>(HttpStatus.INTERNAL_SERVER_ERROR)
            }
        } else {
            LOGGER.error("Trying to leave session by user {}. Session with name {} does not exist.",
                SecurityUtils.getCurrentUsername(), leaveSessionRequest.sessionName)
            ResponseEntity<Any>(HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

    private fun getErrorResponse(e: Exception): ResponseEntity<JSONObject> {
        val json = JSONObject()
        json["cause"] = e.cause
        json["error"] = e.message
        json["exception"] = e.javaClass
        return ResponseEntity(json, HttpStatus.INTERNAL_SERVER_ERROR)
    }

    companion object {

        private val LOGGER = Logger()
    }

}