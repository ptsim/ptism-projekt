package pl.ppteam.doctord.appointment.controller

import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.appointment.domain.Appointment
import pl.ppteam.doctord.appointment.domain.AppointmentCreateRequest
import pl.ppteam.doctord.appointment.domain.AppointmentUpdateRequest
import pl.ppteam.doctord.appointment.domain.AppointmentWithQueue
import pl.ppteam.doctord.appointment.service.AppointmentService
import pl.ppteam.doctord.queue.service.QueueService
import pl.ppteam.doctord.security.SecurityUtils
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/api/v1/appointments")
class AppointmentController(
    private val appointmentService: AppointmentService,
    private val queueService: QueueService
) {

    @GetMapping("/current")
    fun getCurrentUsersActiveAppointment() =
        AppointmentWithQueue(
            appointment = appointmentService.getCurrentUsersActiveAppointment(SecurityUtils.getCurrentUserId()).block(),
            queue = queueService.getByUserId(SecurityUtils.getCurrentUserId())
        )

    @PostMapping
    fun createAppointment(
        @RequestBody request: AppointmentCreateRequest,
    ) = AppointmentWithQueue(
        appointment = appointmentService.createAppointment(request).block(),
        queue = queueService.getByUserId(SecurityUtils.getCurrentUserId())
    )

    @GetMapping("/{id}")
    fun getAppointmentById(
        @PathVariable id: ID,
    ): Mono<Appointment?> = appointmentService.getAppointmentById(id)

    @PutMapping("/{id}")
    fun updateAppointment(
        @PathVariable id: String,
        @RequestBody request: AppointmentUpdateRequest,
    ) = appointmentService.updateAppointmentDescription(request, id)

    @DeleteMapping("/{id}")
    fun deleteAppointment(@PathVariable id: ID) =
        appointmentService.deleteAppointment(id)

    @GetMapping
    fun getAllAppointments() = appointmentService.getAllUsersAppointment(SecurityUtils.getCurrentUserId())
}