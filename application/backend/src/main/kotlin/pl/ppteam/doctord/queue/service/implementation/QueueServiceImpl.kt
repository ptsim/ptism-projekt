package pl.ppteam.doctord.queue.service.implementation

import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirst
import kotlinx.coroutines.reactive.awaitFirstOrDefault
import kotlinx.coroutines.reactive.awaitFirstOrNull
import kotlinx.coroutines.reactive.awaitSingle
import kotlinx.coroutines.reactive.awaitSingleOrNull
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.appointment.domain.Appointment
import pl.ppteam.doctord.queue.domain.QueueEvent
import pl.ppteam.doctord.queue.domain.QueueRecord
import pl.ppteam.doctord.queue.repository.QueueRepository
import pl.ppteam.doctord.queue.service.QueueService
import pl.ppteam.doctord.room.service.RoomService
import pl.ppteam.doctord.util.info

@FlowPreview
@Service
class QueueServiceImpl(
    private val queueRepository: QueueRepository,
    private val queueEventPublisher: ApplicationEventPublisher,
    private val roomService: RoomService
) : QueueService {

    override suspend fun appendAppointment(appointment: Appointment) {
        val queueNumber = findQueueNumber()
        val room = roomService.getDoctorRoom()!!

        val record = QueueRecord(appointment.id!!, appointment.userID, queueNumber, appointment.description, roomID = room.id!!)
            .saveAndAwait()

        info("Save event to repo. Sending QueueEvent.Created. $record")
        queueEventPublisher.publishEvent(QueueEvent.Created(record))
    }

    override suspend fun getFirstFromQueue() = queueRepository
        .findFirstByAppointmentIDNotNullOrderByNumberAsc()
        .awaitFirstOrNull()

    override suspend fun getRecordById(recordId: ID): QueueRecord? = queueRepository
        .findById(recordId)
        .awaitSingleOrNull()

    override fun letIn(recordId: ID) {
        queueRepository.findById(recordId)
            .subscribe { queueEventPublisher.publishEvent(QueueEvent.LetIn(it)) }
    }

    override suspend fun isUserInQueue(userId: ID): Boolean =
        queueRepository.existsByUserID(userId).awaitFirst()

    override fun leaveQueue(userId: ID) {
        queueRepository.findByUserID(userId).doOnNext { record ->
            queueRepository.delete(record).subscribe()
            queueEventPublisher.publishEvent(QueueEvent.Deleted(record))
        }.subscribe()
    }

    override fun getByUserId(userId: ID): QueueRecord? {
        return queueRepository.findByUserID(userId).block()
    }

    private suspend fun findQueueNumber() = queueRepository
        .findFirstByAppointmentIDNotNullOrderByNumberDesc()
        .map { it.number }
        .awaitFirstOrDefault(-1)
        .inc()

    private suspend fun QueueRecord.saveAndAwait() = queueRepository
        .save(this)
        .awaitSingle()

}