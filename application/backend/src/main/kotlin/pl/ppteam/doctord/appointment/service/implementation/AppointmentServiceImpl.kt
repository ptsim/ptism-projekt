package pl.ppteam.doctord.appointment.service.implementation

import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.appointment.domain.Appointment
import pl.ppteam.doctord.appointment.domain.AppointmentCreateRequest
import pl.ppteam.doctord.appointment.domain.AppointmentEvent
import pl.ppteam.doctord.appointment.domain.AppointmentUpdateRequest
import pl.ppteam.doctord.appointment.domain.toDomain
import pl.ppteam.doctord.appointment.repository.AppointmentRepository
import pl.ppteam.doctord.appointment.service.AppointmentService
import pl.ppteam.doctord.room.service.RoomService
import pl.ppteam.doctord.security.SecurityUtils
import pl.ppteam.doctord.util.info
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.Instant

@Service
class AppointmentServiceImpl(
    private val appointmentRepository: AppointmentRepository,
    private val eventPublisher: ApplicationEventPublisher,
    private val roomService: RoomService
) : AppointmentService {

    override fun getCurrentUsersActiveAppointment(userID: ID): Mono<Appointment?> =
        appointmentRepository
            .findFirstByIsHandledFalseAndUserID(SecurityUtils.getCurrentUserId())

    override fun updateAppointmentDescription(request: AppointmentUpdateRequest, id: ID): Mono<Appointment> {
        val doctorId = SecurityUtils.getCurrentUserId()
        return appointmentRepository.findById(id)
            .map {
                it.copy(
                    handledAt = Instant.now(),
                    handledBy = doctorId,
                    isHandled = true,
                    recommendations = request.recommendations
                )
            }
            .flatMap {
                appointmentRepository.save(it)
            }
    }

    override fun createAppointment(request: AppointmentCreateRequest): Mono<Appointment?> {
        roomService.getDoctorRoom() ?: throw IllegalStateException("No doctor room present")

        val currentUser = SecurityUtils.getCurrentUserId()
        return appointmentRepository
            .findFirstByIsHandledFalseAndUserID(currentUser)
            .switchIfEmpty(appointmentRepository.save(request.toDomain(currentUser)))
            .doOnNext { appointment ->
                if (appointment != null) eventPublisher.publishEvent(AppointmentEvent.Created(appointment))
                    .info("Appointment created. Published appointment event $appointment")
            }
    }

    override fun deleteAppointment(id: ID): Mono<Void> =
        appointmentRepository.deleteById(id)

    override fun getAppointmentById(id: ID): Mono<Appointment?> =
        appointmentRepository.findById(id)

    override fun getAllUsersAppointment(currentUserId: String): Flux<Appointment> =
        appointmentRepository.findAllByUserID(currentUserId)
}
