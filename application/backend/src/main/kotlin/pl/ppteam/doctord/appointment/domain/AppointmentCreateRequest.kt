package pl.ppteam.doctord.appointment.domain

import pl.ppteam.doctord.ID

data class AppointmentCreateRequest(
    val description: String,
)

fun AppointmentCreateRequest.toDomain(userId: ID) = Appointment(userId, description)