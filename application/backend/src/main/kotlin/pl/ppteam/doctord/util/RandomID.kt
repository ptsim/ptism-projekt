package pl.ppteam.doctord.util

import java.util.UUID

fun randomID(): String = UUID.randomUUID().toString()