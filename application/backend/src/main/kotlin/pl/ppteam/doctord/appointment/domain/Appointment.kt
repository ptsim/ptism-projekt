package pl.ppteam.doctord.appointment.domain

import org.springframework.data.annotation.TypeAlias
import org.springframework.data.mongodb.core.index.IndexDirection
import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import pl.ppteam.doctord.ID
import java.time.Instant

@Document("appointments")
@TypeAlias("Appointment")
data class Appointment(
    @Indexed
    val userID: ID,

    val description: String,

    @Indexed
    val isHandled: Boolean = false,

    val handledAt: Instant? = null,

    val handledBy: ID? = null,

    @Indexed
    val isExpired: Boolean = false,

    @Indexed(direction = IndexDirection.ASCENDING)
    val createdAt: Instant = Instant.now(),

    val recommendations: String? = null,

    var id: ID? = null,
)

