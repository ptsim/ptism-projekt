package pl.ppteam.doctord.appointment.domain

import pl.ppteam.doctord.queue.domain.QueueRecord

data class AppointmentWithQueue(
    val appointment: Appointment?,
    val queue: QueueRecord?
)