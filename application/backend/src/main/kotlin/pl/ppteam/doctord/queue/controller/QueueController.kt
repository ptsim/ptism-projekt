package pl.ppteam.doctord.queue.controller

import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.queue.service.QueueService
import pl.ppteam.doctord.security.KeycloakRoles

@RestController
@RequestMapping("/api/v1/queue")
class QueueController(
    private val service: QueueService,
) {

    @PostMapping("/{recordId}")
    @Secured(KeycloakRoles.ROLE_DOCTOR)
    fun deleteRecord(@PathVariable recordId: ID) = service.letIn(recordId)
}