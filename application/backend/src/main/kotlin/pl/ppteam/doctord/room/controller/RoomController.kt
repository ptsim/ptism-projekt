package pl.ppteam.doctord.room.controller

import org.springframework.http.HttpStatus
import org.springframework.security.access.annotation.Secured
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import pl.ppteam.doctord.room.controller.request.CreateRoomRequestKt
import pl.ppteam.doctord.room.domain.RoomKt
import pl.ppteam.doctord.room.service.RoomService
import pl.ppteam.doctord.security.KeycloakRoles
import pl.ppteam.doctord.security.SecurityUtils
import pl.ppteam.doctord.utils.Logger
import reactor.core.publisher.Mono
import javax.validation.Valid

@RestController
@RequestMapping("/api/room")
class RoomController(
    private val roomService: RoomService,
) {
    /**
     * Creates and saves new room. Issuer is automatically added as participant.
     *
     * @param request data required to create room
     * @return HttpStatus 201 - id of newly created room
     */
    @ResponseStatus(HttpStatus.CREATED)
    @Secured(KeycloakRoles.ROLE_DOCTOR)
    @PostMapping
    fun createRoom(
        @RequestBody request: @Valid CreateRoomRequestKt,
    ): Mono<RoomKt> {
        LOGGER.info("Creating new room by user {} with name: {}.", SecurityUtils.getCurrentUsername(), request.name)
        return roomService
            .createRoom(request, SecurityUtils.getCurrentUsername())
    }

    @GetMapping("/{roomId}") // TODO: tutaj sprawdzać czy pacjent ma ofertę połaczenia -> jeśli tak to 200, jeśli nie to jakieś 400
    fun getRoomDetails(@PathVariable("roomId") roomId: String): Mono<RoomKt> {
        LOGGER.info("Fetching room details with id {} by user with name: {}.", roomId, SecurityUtils.getCurrentUsername())
        return roomService.getRoomDetails(roomId)
    }

    @Secured(KeycloakRoles.ROLE_DOCTOR)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{roomId}")
    fun deleteRoom(@PathVariable("roomId") roomId: String): Mono<Void> {
        LOGGER.info("Deleting room with id {} by user with name {}.", roomId, SecurityUtils.getCurrentUsername())
        val isAdmin = SecurityUtils.hasCurrentUserRole(KeycloakRoles.ROLE_DOCTOR)
        val currentUsername = SecurityUtils.getCurrentUsername()
        return roomService.deleteRoom(roomId, currentUsername, isAdmin)
    }

    @Secured(KeycloakRoles.ROLE_DOCTOR)
    @PutMapping("/{roomId}")
    fun updateRoom(
        @PathVariable roomId:String,
        @RequestBody roomRequest: CreateRoomRequestKt,
    ): Mono<RoomKt> {
        LOGGER.info("Updating room with id {} by user with name {}.", roomId, SecurityUtils.getCurrentUsername())
        return roomService.update(roomId, roomRequest, SecurityUtils.getCurrentUsername())
    }

    @GetMapping("/{roomName}/exist")
    fun checkIfNameExists(
        @PathVariable roomName: String,
    ): Mono<Boolean> {
        LOGGER.info("Checking if room with name {} exists by user with name {}.", roomName, SecurityUtils.getCurrentUsername())
        return roomService.checkIfRoomExists(roomName)
    }

    companion object {
        private val LOGGER = Logger()
    }
}