package pl.ppteam.doctord.room.service.implementation

import lombok.RequiredArgsConstructor
import org.springframework.stereotype.Service
import pl.ppteam.doctord.controller.error.exception.RoomDeletionException
import pl.ppteam.doctord.controller.error.exception.RoomNotFoundException
import pl.ppteam.doctord.entity.notification.NotificationType
import pl.ppteam.doctord.room.controller.request.CreateRoomRequestKt
import pl.ppteam.doctord.room.controller.request.toRoom
import pl.ppteam.doctord.room.domain.RoomKt
import pl.ppteam.doctord.room.repository.RoomRepository
import pl.ppteam.doctord.room.service.RoomService
import pl.ppteam.doctord.security.SecurityUtils
import pl.ppteam.doctord.service.NotificationsService
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono

@Service
@RequiredArgsConstructor
class RoomServiceImpl(
    private val roomRepository: RoomRepository,
    private val notificationsService: NotificationsService,
) : RoomService {

    override fun createRoom(request: CreateRoomRequestKt, username: String): Mono<RoomKt> =
        roomRepository.existsByIdNotNull()
            .flatMap {
                if (it) roomRepository.findAll().toMono()
                else roomRepository.save(request.toRoom(username))
            }

    override fun getRoomDetails(roomId: String): Mono<RoomKt> {
        return roomRepository
            .findById(roomId)
            .switchIfEmpty(Mono.error(RoomNotFoundException()))
    }

    override fun deleteRoom(roomId: String, issuer: String, isAdmin: Boolean): Mono<Void> {
        notificationsService.sendNotification(SecurityUtils.getCurrentUsername(), NotificationType.ROOM_DELETED)
        return roomRepository
            .findById(roomId)
            .switchIfEmpty(
                Mono.error(RoomNotFoundException())
            )
            .flatMap { room ->
                if (isAdmin || room.isOwnedBy(issuer))
                    return@flatMap roomRepository.deleteById(roomId)
                else
                    return@flatMap Mono.error<Void>(RoomDeletionException())
            }
    }

    override fun update(roomId: String, roomRequest: CreateRoomRequestKt, username: String): Mono<RoomKt> {
        return roomRepository
            .findByIdAndOwnerUsername(roomId, username)
            .switchIfEmpty(Mono.error(RoomNotFoundException()))
            .flatMap { room ->
                val update = roomRequest.toRoom(room.ownerUsername, room.id)
                roomRepository.save(update)
            }
    }

    override fun checkIfRoomExists(roomName: String): Mono<Boolean> {
        return roomRepository
            .existsRoomByName(roomName)
    }

    override fun getDoctorRoom(): RoomKt? = roomRepository.findAll().blockFirst()
}