package pl.ppteam.doctord.appointment.repository

import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import pl.ppteam.doctord.ID
import pl.ppteam.doctord.appointment.domain.Appointment
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface AppointmentRepository : ReactiveMongoRepository<Appointment, ID> {

    fun findFirstByIsHandledFalseAndUserID(userID: ID): Mono<Appointment?>
    fun findAllByUserID(currentUserId: String): Flux<Appointment>

}