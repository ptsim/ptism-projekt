package pl.ppteam.doctord

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.launch
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration
import org.springframework.boot.runApplication
import org.springframework.context.event.ContextRefreshedEvent
import org.springframework.context.event.EventListener
import org.springframework.data.mapping.context.MappingContext
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.index.IndexDefinition
import org.springframework.data.mongodb.core.index.IndexResolver
import org.springframework.data.mongodb.core.index.MongoPersistentEntityIndexResolver
import org.springframework.data.mongodb.core.index.ReactiveIndexOperations
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.MongoPersistentEntity
import org.springframework.data.mongodb.core.mapping.MongoPersistentProperty
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.config.EnableWebFlux

@SpringBootApplication
@EnableReactiveMongoRepositories
class DoctordApplication

fun main(args: Array<String>) {
    runApplication<DoctordApplication>(*args)
}

@Component
// TODO: not initialziing properly
class IndexMongoCreator(
    private val mongoTemplate: ReactiveMongoTemplate,
) {

    @EventListener(ContextRefreshedEvent::class)
    fun initIndicesAfterStartup() {
        val mappingContext: MappingContext<out MongoPersistentEntity<*>?, MongoPersistentProperty> = mongoTemplate
            .converter.mappingContext

        val resolver: IndexResolver = MongoPersistentEntityIndexResolver(mappingContext)
        mappingContext.persistentEntities
            .filter { it!!.isAnnotationPresent(Document::class.java) }
            .forEach {
                val indexOps: ReactiveIndexOperations = mongoTemplate.indexOps(it!!.type)
                resolver.resolveIndexFor(it.type)
                    .forEach { indexDefinition: IndexDefinition? -> indexOps.ensureIndex(indexDefinition!!) }
            }
    }
}

data class IntClazz(
    val value: Int
)

@RestController
@RequestMapping("/hehe")
class TestCtrl{

    private val flow = MutableSharedFlow<IntClazz>()

    private var ctr = 0

    init {
        val job = SupervisorJob()
        val cts = CoroutineScope(job)

        cts.launch {
            while(true){
                flow.emit(IntClazz(ctr))
                ctr++
                delay(500)
            }
        }
    }

    @GetMapping(produces = [MediaType.APPLICATION_STREAM_JSON_VALUE])
    fun get() = flow
}