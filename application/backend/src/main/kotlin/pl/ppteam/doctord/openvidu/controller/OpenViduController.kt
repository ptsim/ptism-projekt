package pl.ppteam.doctord.openvidu.controller

import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.ppteam.doctord.openvidu.controller.request.LeaveSessionRequest
import pl.ppteam.doctord.openvidu.controller.request.SessionRequest
import pl.ppteam.doctord.queue.service.QueueService
import pl.ppteam.doctord.security.SecurityUtils
import pl.ppteam.doctord.service.OpenViduService
import pl.ppteam.doctord.utils.Logger
import javax.validation.Valid

@RestController
@RequestMapping("/api/session")
class OpenViduController(
    private val openViduService: OpenViduService,
    private val queueService: QueueService
) {

    /**
     * Joining a call session with another user.
     *
     * @param sessionRequest body of a session join request
     * @return token - token of OpenVidu session
     */
    @PostMapping("/join")
    fun getToken(@RequestBody sessionRequest: @Valid SessionRequest): ResponseEntity<*> {
        LOGGER.info("Getting a token from OpenVidu Server by user {} | Session name: {}.",
            SecurityUtils.getCurrentUsername(), sessionRequest.sessionName)

        queueService.leaveQueue(SecurityUtils.getCurrentUserId())

        return openViduService.getToken(sessionRequest)
    }

    /**
     * Leaving a call session with another user.
     *
     * @param leaveSessionRequest body of a session leave request
     * @return HttpStatus 201 - session left
     */
    @PostMapping("/leave")
    fun removeUser(@RequestBody leaveSessionRequest: LeaveSessionRequest): ResponseEntity<*> {
        LOGGER.info("Removing a user {} from session | Session name: {}.",
            SecurityUtils.getCurrentUsername(), leaveSessionRequest.sessionName)

        return openViduService.removeUser(leaveSessionRequest)
    }

    companion object {

        private val LOGGER = Logger()
    }
}