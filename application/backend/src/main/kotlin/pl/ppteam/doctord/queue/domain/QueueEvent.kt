package pl.ppteam.doctord.queue.domain

sealed class QueueEvent {
    data class Created(val record: QueueRecord) : QueueEvent()

    data class Deleted(val record: QueueRecord) : QueueEvent()

    data class LetIn(val record: QueueRecord) : QueueEvent()

}