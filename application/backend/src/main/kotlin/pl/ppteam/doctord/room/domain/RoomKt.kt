package pl.ppteam.doctord.room.domain

import org.springframework.data.annotation.TypeAlias
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document("rooms")
@TypeAlias("Room")
data class RoomKt(
    val name: String,
    val ownerUsername: String,
    val workStart: Instant,
    val workEnd: Instant,
    val createdAt: Instant = Instant.now(),
    var id: String? = null,
) {

    fun isOwnedBy(user: String?): Boolean {
        return ownerUsername.equals(user, ignoreCase = true)
    }

    val isFinished: Boolean
        get() = Instant.now().isAfter(workEnd)
}