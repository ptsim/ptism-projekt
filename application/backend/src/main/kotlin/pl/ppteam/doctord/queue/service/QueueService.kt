package pl.ppteam.doctord.queue.service

import pl.ppteam.doctord.ID
import pl.ppteam.doctord.appointment.domain.Appointment
import pl.ppteam.doctord.queue.domain.QueueRecord

interface QueueService {

    suspend fun appendAppointment(appointment: Appointment)

    suspend fun getFirstFromQueue(): QueueRecord?

    suspend fun getRecordById(recordId: ID): QueueRecord?

    fun letIn(recordId: ID)

    suspend fun isUserInQueue(userId: ID): Boolean

    fun leaveQueue(userId: ID)

    fun getByUserId(userId: ID): QueueRecord?
}

