package pl.ppteam.doctord.room.controller.request

import pl.ppteam.doctord.room.domain.RoomKt
import java.time.Instant

data class CreateRoomRequestKt(
    val name: String,
    val workStart: Instant,
    val workEnd: Instant,
)

fun CreateRoomRequestKt.toRoom(ownerUsername: String, id: String? = null) =
    RoomKt(
        name = name,
        ownerUsername = ownerUsername,
        workStart = workStart,
        workEnd = workEnd,
        id = id
    )