package pl.ppteam.doctord.openvidu.controller.request

import javax.validation.constraints.NotBlank

data class LeaveSessionRequest(
    var sessionName: @NotBlank String,
    var token: @NotBlank String,
)