package pl.ppteam.doctord.queue.listener

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactive.awaitFirstOrNull
import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.event.EventListener
import org.springframework.data.mongodb.core.ReactiveMongoTemplate
import org.springframework.data.mongodb.core.query.Update
import org.springframework.data.mongodb.core.query.isEqualTo
import org.springframework.data.mongodb.core.query.where
import org.springframework.data.mongodb.core.update
import org.springframework.messaging.simp.SimpMessagingTemplate
import org.springframework.stereotype.Component
import pl.ppteam.doctord.queue.domain.QueueEvent
import pl.ppteam.doctord.queue.domain.QueueRecord
import pl.ppteam.doctord.queue.repository.QueueRepository
import pl.ppteam.doctord.room.service.RoomService
import pl.ppteam.doctord.util.info

@Component
class QueueEventListener(
    private val queueCoroutineScope: CoroutineScope,
    private val queueRepository: QueueRepository,
    private val roomService: RoomService,
    private val mongoTemplate: ReactiveMongoTemplate,
    private val simpMessageTemplate: SimpMessagingTemplate
) {

    @EventListener
    fun queueRecordCreated(evt: QueueEvent.Created) = queueCoroutineScope.launch {
        info("Handling QueueEvent.Created: publishing updated queue $evt.")

        evt.record.sendToUser()

        val wholeQueue = queueRepository.findAll().collectSortedList { o1, o2 -> o1.number.compareTo(o2.number) }
        simpMessageTemplate.convertAndSend("/doctor-queue", wholeQueue)
    }

    @EventListener
    fun queueRecordDeleted(evt: QueueEvent.Deleted) = queueCoroutineScope.launch {
        val record = evt.record

        info("Handling QueueEvent.Deleted $evt")
        mongoTemplate.update<QueueRecord>()
            .matching(where(QueueRecord::number).gt(record.number))
            .apply(Update().inc("number", -1))
            .all()
            .awaitFirstOrNull()

        val wholeQueue = queueRepository.findAll()
            .asFlow()
            .onEach { it.sendToUser() }
            .toList()

        simpMessageTemplate.convertAndSend("/doctor-queue", wholeQueue)
    }

    @EventListener
    fun queueRecordLetIn(evt: QueueEvent.LetIn) = queueCoroutineScope.launch {
        roomService.getDoctorRoom()?.let { room ->
            val (record) = evt

            queueRepository.save(record.copy(roomID = room.id!!))

            record.sendToUser()
        }
    }

    private fun QueueRecord.sendToUser() = simpMessageTemplate.convertAndSendToUser(this.userID, "/patient-queue", this)
}