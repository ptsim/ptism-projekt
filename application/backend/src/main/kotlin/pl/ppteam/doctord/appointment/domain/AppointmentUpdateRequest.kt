package pl.ppteam.doctord.appointment.domain

data class AppointmentUpdateRequest(
    val recommendations: String
)
