package pl.ppteam.doctord.appointment.configuration

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class AppointmentConfiguration {

    @Bean
    fun appointmentCreatedEventScope() = CoroutineScope(SupervisorJob() + Dispatchers.IO)

}