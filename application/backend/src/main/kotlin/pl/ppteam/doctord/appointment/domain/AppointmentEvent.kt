package pl.ppteam.doctord.appointment.domain

sealed class AppointmentEvent {

    data class Created(val appointment: Appointment) : AppointmentEvent()
}