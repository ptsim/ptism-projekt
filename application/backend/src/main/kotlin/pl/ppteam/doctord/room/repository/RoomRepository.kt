package pl.ppteam.doctord.room.repository

import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import pl.ppteam.doctord.room.domain.RoomKt
import reactor.core.publisher.Mono

@Repository
interface RoomRepository : ReactiveMongoRepository<RoomKt, String> {

    fun findByIdAndOwnerUsername(id: String, username: String): Mono<RoomKt>

    fun existsRoomByName(name: String): Mono<Boolean>

    fun existsByIdNotNull(): Mono<Boolean>
}