package pl.ppteam.doctord.room.service

import pl.ppteam.doctord.room.controller.request.CreateRoomRequestKt
import pl.ppteam.doctord.room.domain.RoomKt
import reactor.core.publisher.Mono

interface RoomService {

    fun createRoom(request: CreateRoomRequestKt, username: String): Mono<RoomKt>
    fun getRoomDetails(roomId: String): Mono<RoomKt>
    fun deleteRoom(roomId: String, issuer: String, isAdmin: Boolean): Mono<Void>
    fun update(roomId: String, roomRequest: CreateRoomRequestKt, username: String): Mono<RoomKt>
    fun checkIfRoomExists(roomName: String): Mono<Boolean>

    fun getDoctorRoom(): RoomKt?
}