package pl.ppteam.doctord.openvidu.controller.request

import io.openvidu.java.client.OpenViduRole

data class SessionRequest (
    val username: String,

    val sessionName: String,

    val role: OpenViduRole
)