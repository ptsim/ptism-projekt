package pl.ppteam.doctord.queue.domain

import org.springframework.data.mongodb.core.index.Indexed
import org.springframework.data.mongodb.core.mapping.Document
import pl.ppteam.doctord.ID

@Document("QueueRecord")
data class QueueRecord(

    @Indexed(unique = true)
    val appointmentID: ID,

    @Indexed(unique = true)
    val userID: ID,

    @Indexed(unique = true)
    val number: Long,

    val appointmentDescription: String,

    var id: ID? = null,

    val roomID: String? = null,

)