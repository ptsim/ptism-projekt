package pl.ppteam.doctord.appointment.service

import pl.ppteam.doctord.ID
import pl.ppteam.doctord.appointment.domain.Appointment
import pl.ppteam.doctord.appointment.domain.AppointmentCreateRequest
import pl.ppteam.doctord.appointment.domain.AppointmentUpdateRequest
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface AppointmentService {

    fun getCurrentUsersActiveAppointment(userID: ID): Mono<Appointment?>
    fun updateAppointmentDescription(request: AppointmentUpdateRequest, id: ID): Mono<Appointment>
    fun createAppointment(request: AppointmentCreateRequest): Mono<Appointment?>
    fun deleteAppointment(id: ID): Mono<Void>
    fun getAppointmentById(id: ID): Mono<Appointment?>
    fun getAllUsersAppointment(currentUserId: String): Flux<Appointment>
}

