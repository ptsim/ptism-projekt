version: "3.3"

services:
  backend:
    image: "registry.gitlab.com/ptsim/ptism-projekt:latest"
    expose:
      - "8444"
    networks:
      - hobbit_hole
    depends_on:
      - mongodb
      - keycloak
      - openvidu
    environment:
      - SPRING_PROFILES_ACTIVE=dev
      - SPRING_DATA_MONGODB_HOST=mongodb
      - SPRING_DATA_MONGODB_PORT=27017
      - SPRING_DATA_MONGODB_DATABASE=stream_app_dev
      - KEYCLOAK_AUTH_SERVER_URL=https://www.ptism.test/auth
      - LOGGING_LEVEL_ROOT=info
    volumes:
      - ./ssl/keycloak.test/cert.pem:/tmp/nginx.crt # /tmp/nginx.crt is imported in entrypoint.sh

  frontend:
    restart: always
    build:
      context: ../application/frontend
      dockerfile: Dockerfile
    expose:
      - 8081
    networks:
      - hobbit_hole

  mongodb:
    image: mongo:4.2
    ports:
      - "27017:27017" # TODO: hide in production
    networks:
      - hobbit_hole
    volumes:
      - mongodata:/data/db

  keycloak:
    image: jboss/keycloak:9.0.0
    restart: on-failure
    command:
      - "-b"
      - "0.0.0.0"
      - "-Dkeycloak.migration.action=import"
      - "-Dkeycloak.migration.provider=dir"
      - "-Dkeycloak.migration.dir=/config"
      - "-Dkeycloak.migration.strategy=OVERWRITE_EXISTING"
    networks:
      hobbit_hole:
        aliases:
          - ptism.test
    volumes:
      - ./keycloak/config:/config/
      - ./keycloak/login-theme/:/opt/jboss/keycloak/themes/login-theme/
      - ./keycloak/keys/certificate.crt:/etc/x509/https/tls.crt
      - ./keycloak/keys/privateKey.key:/etc/x509/https/tls.key
    environment:
      - KEYCLOAK_USER=admin
      - KEYCLOAK_PASSWORD=password
      - DB_VENDOR=postgres
      - DB_USER=admin
      - DB_PASSWORD=password
      - DB_ADDR=keycloak-db
      - DB_PORT=5432
      - DB_DATABASE=keycloakdb
    expose:
      - "8080" # TODO: remove in production
      - "8443"
    depends_on:
      - keycloak-db

  keycloak-db:
    image: postgres:10
    environment:
      POSTGRES_USER: admin
      POSTGRES_PASSWORD: password
      POSTGRES_DB: keycloakdb
    volumes:
      - pgdata:/var/lib/postgresql/data
    networks:
      - hobbit_hole

  openvidu:
    image: openvidu/openvidu-server-kms:2.13.0
    environment:
      - OPENVIDU_SECRET=YOUR_SECRET
      - OPENVIDU_DOMAIN_OR_PUBLIC_IP=www.ptism.test/openvidu-api/
      - COTURN_REDIS_IP=redis
      - COTURN_REDIS_PASSWORD=YOUR_SECRET
      - COTURN_IP=coturn
    expose:
      - 4443
    networks:
      - hobbit_hole

  proxy:
    image: nginx:alpine
    restart: always
    depends_on:
      - backend
      - openvidu
      - keycloak
    ports:
      - 80:80
      - 443:443
    networks:
      hobbit_hole:
        aliases:
          - www.ptism.test
    volumes:
      - ./nginx/:/etc/nginx/
      - ./ssl/:/ssl/

  redis:
    image: openvidu/openvidu-redis:1.0.0
    restart: always
    ports:
      - 6379:6379
    networks:
      hobbit_hole:
    environment:
      - REDIS_PASSWORD=YOUR_SECRET
      - REDIS_BINDING=redis

  coturn:
    image: openvidu/openvidu-coturn:1.0.0
    restart: on-failure
    ports:
      - 3478:3478
    depends_on:
      - redis
    networks:
      hobbit_hole:
    environment:
      - REDIS_IP=redis
      - TURN_LISTEN_PORT=3478
      - DB_NAME=0
      - DB_PASSWORD=YOUR_SECRET
      - MIN_PORT=57001
      - MAX_PORT=65535

volumes:
  pgdata:
  mongodata:

networks:
  hobbit_hole:
